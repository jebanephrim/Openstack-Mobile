package com.poc.openstack;


import java.net.MalformedURLException;
import java.net.URL;
import java.util.HashMap;
import java.util.Map;

import org.apache.http.HttpStatus;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import com.android.volley.AuthFailureError;
import com.android.volley.NetworkResponse;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.Response.ErrorListener;
import com.android.volley.Response.Listener;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;
import com.poc.openstack.CreateServerActivity.SetAuthTokenHeader;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Fragment;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Typeface;
import android.os.AsyncTask;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

public class AccessSecurityDetailsActivity extends Activity {
	
	public AccessSecurityDetailsActivity(){}
	private String ip;
	private String url_services;
	SetAuthTokenHeader rebootrequest = null ;
	SetAuthTokenHeader changepassrequest = null ;
	SetAuthTokenHeader deleterequest;
	 private String TenantId;
	 private String SecurityGroupId;
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
        setContentView(R.layout.access_security_details);
        //TenantId=getIntent().getStringExtra("tenant_id");
		SecurityGroupId=getIntent().getStringExtra("security_id");
		
		setBaseURL();
        getServerFlavorDetails(TenantId,SecurityGroupId);
       
    }
	
	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
	    // Inflate the menu items for use in the action bar
	    MenuInflater inflater = getMenuInflater();
	    inflater.inflate(R.menu.secrule, menu);
	    return super.onCreateOptionsMenu(menu);
	}
	
	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
	    // Handle presses on the action bar items
	    switch (item.getItemId()) {
	     
	        case R.id.action_addrule:
	        	//changePasswordDialog(TenantId,ServerId);
	        	Intent intent= new Intent(getApplicationContext(),CreateSecurityGroupRuleActivity.class);
	        	intent.putExtra("secgroup_id", SecurityGroupId);
		        startActivity(intent);
	            return true;
	            
	        case R.id.action_del_sec:
	        	//changePasswordDialog(TenantId,ServerId);
	        	deleteSecGroup();
	            return true;
	       
	        default:
	            return super.onOptionsItemSelected(item);
	    }
	}
	
	
	 private void deleteSecGroup() {
		// TODO Auto-generated method stub
		 AlertDialog.Builder alert = new AlertDialog.Builder(this);

			alert.setTitle("Delete Security Group");
			alert.setMessage("Are you sure?");



			alert.setPositiveButton("Yes", new DialogInterface.OnClickListener() {
			public void onClick(DialogInterface dialog, int whichButton) {
			  try {
				RequestQueue requestQueue = Volley.newRequestQueue(getApplicationContext());    

						URL url = new URL(url_services+"/security-groups/"+SecurityGroupId);

						deleterequest= new  SetAuthTokenHeader(
					            Request.Method.DELETE,
					            url.toString().trim(),null,
					            deletesuccessListener(),
					            deleteErrorListener());
						
						requestQueue.add(deleterequest);

			        } catch (Exception e) {
			        	
			            e.printStackTrace();
			            
			    
			        }
			  }
			});

			alert.setNegativeButton("No", new DialogInterface.OnClickListener() {
			  public void onClick(DialogInterface dialog, int whichButton) {
			    // Canceled.
			  }
			});

			alert.show();
			
		}

		private Response.Listener<String> deletesuccessListener() {
		    return new Response.Listener<String>() {
		        @Override
		        public void onResponse(String response) {
		        	Toast.makeText(getApplicationContext(), "Delete Success!", Toast.LENGTH_SHORT).show();
		        	Intent intent= new Intent(getApplicationContext(),MainActivity.class);
			        startActivityForResult(intent,0);	 
		     
		        	               }
		    };

		}


		private Response.ErrorListener deleteErrorListener() {
		    return new Response.ErrorListener() {
		        @Override
		        public void onErrorResponse(VolleyError error) {
		        	error.printStackTrace();
		        	 if(deleterequest.getStatusCode()==204){
		        		 	Toast.makeText(getApplicationContext(), "Delete Security Group Success!", Toast.LENGTH_SHORT).show(); 
		        		 	Intent intent= new Intent(getApplicationContext(),MainActivity.class);
					        startActivityForResult(intent,0);	 
		        	 }
		        	 else
		        			Toast.makeText(getApplicationContext(), "Delete Security Group Failed!", Toast.LENGTH_SHORT).show();
		        	//Toast.makeText(getApplicationContext(), "Delete Failed!", Toast.LENGTH_SHORT).show();
		        }
		    };
		}


	private void setBaseURL()
		{
		    SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(getApplicationContext());
	        String host_ip = preferences.getString("host_ip","");
	        if(!host_ip.equalsIgnoreCase(""))
	        {
	          ip= host_ip;
	          url_services="http://"+ip+":9696/v2.0";
	        }
	        else
	        {
	      
	       Toast.makeText(getApplicationContext(), "Please configure openstack server to proceed", Toast.LENGTH_SHORT).show();
	       Intent intent= new Intent(this,SettingsActivity.class);
	       startActivityForResult(intent,0);
	        }
		}
	 
	void getServerFlavorDetails(String tenantId, String SecId)
	{
		try {
			RequestQueue requestQueue = Volley.newRequestQueue(getApplicationContext());    
			 JsonObjectRequest securityGroupDetailsRequest = null ;	
	
				URL url = new URL(url_services+"/security-groups/"+SecId);
							
				securityGroupDetailsRequest= new  SetAuthTokenHeader(
			            Request.Method.GET,
			            url.toString().trim(),null,
			            serverSuccessListener(),
			            serverErrorListener());
				
				requestQueue.add(securityGroupDetailsRequest);

	        } catch (Exception e) {
	        	
                e.printStackTrace();
	            
	    
	        }
	}
	private Response.Listener<JSONObject> serverSuccessListener() {
        return new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
            
            	TextView txt=(TextView)findViewById(R.id.secdetails);
            	Typeface typeface = Typeface.createFromAsset(getAssets(), "font/Roboto-Medium.ttf");
            	txt.setTypeface(typeface);
               //System.out.println(response.toString());
            	try {
            		 JSONObject json= response.getJSONObject("security_group"); 
            		String server_name=json.getString("name");
            	 	String server_id=json.getString("id");
            	 	String status=json.getString("description");
            	 	JSONArray rules=json.getJSONArray("security_group_rules");
            	 	
            	 	int length=rules.length();
            	 
            	 txt.setText("Name:\n "+server_name+"\n"+
            	 	          "Id:\n "+server_id+"\n"+
            	 	          "Description:\n "+status+"\n"+
            	 	          "Rules:\n"
            	 	          );
            	 if(length==0)
            		 txt.append("Rules not available");
            	 else
            	 {
            	 for(int i=0;i<length;i++)
            	 {
            	
            		 JSONObject rulesObj= rules.getJSONObject(i);
            		txt.append("\n"+(i+1)+".\n");
            		txt.append("ID: "+rulesObj.getString("id")+"\n");
            		txt.append("Direction: "+rulesObj.getString("direction")+"\n");
            		txt.append("Ethertype: "+rulesObj.getString("ethertype")+"\n");
            		txt.append("Port Range Max: "+rulesObj.getString("port_range_max")+"\n");
            		txt.append("Port Range Min: "+rulesObj.getString("port_range_min")+"\n");
            		
            	 }
            	 }
            		//System.out.println("Hello!!"+output);
            	 	
					
				} catch (JSONException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
         
            	               }
        };
    
    }
    
    
    private Response.ErrorListener serverErrorListener() {
        return new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
            	error.printStackTrace();
            	Toast.makeText(getApplicationContext(), "Fetch Failed!", Toast.LENGTH_SHORT).show();
            }
        };
    }
	
	
	
    
	
	public class SetAuthTokenHeader extends JsonObjectRequest 
	{
		int mStatusCode;
	 public SetAuthTokenHeader(int method, String url, JSONObject jsonRequest,Listener listener, ErrorListener errorListener)
	 {
	   super(method, url, jsonRequest, listener, errorListener);
	 }

	 @Override
	 public Map getHeaders() throws AuthFailureError {
	   Map headers = new HashMap();
	   SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(getApplicationContext());
	   String token = preferences.getString("auth_token","");
	   headers.put("X-Auth-Token", token);
	   return headers;
	 }
	 
	 public int getStatusCode() {
	        
			return mStatusCode;
	    }

	    @Override
	    protected Response<JSONObject> parseNetworkResponse(NetworkResponse response) {
	        mStatusCode = response.statusCode;
	        return super.parseNetworkResponse(response);
	    }
	    

	}
	

}
