package com.poc.openstack;



import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.logging.Logger;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.Response.ErrorListener;
import com.android.volley.Response.Listener;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;
import com.poc.openstack.ImagesFragment.SetAuthTokenHeader;
import com.poc.openstack.adapter.CustomListViewAdapter;
import com.poc.openstack.adapter.FlavorsSpinnerAdapter;
import com.poc.openstack.adapter.ImagesSpinnerAdapter;
import com.poc.openstack.model.FlavorsSpinnerItem;
import com.poc.openstack.model.ImagesSpinnerItem;
import com.poc.openstack.model.ServerListItem;

import android.app.Activity;
import android.content.Intent;
import android.content.SharedPreferences;

import android.os.Bundle;
import android.preference.PreferenceManager;

import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;

import android.widget.Button;

import android.widget.EditText;
import android.widget.ListView;

import android.widget.Toast;

public class CreateSecurityGroupActivity extends Activity {
	
	public CreateSecurityGroupActivity(){}
	   private static final int RESULTS_PAGE_SIZE = 10;
	   String ip;
	String url_services;
	RequestQueue requestQueue;

	 
	  ListView listView ;
	  String tenant_id;
	  ImagesSpinnerAdapter ispinadapter ;
	  FlavorsSpinnerAdapter fspinadapter;

CustomListViewAdapter adapter;
JSONArray responsejson;

    public void onCreate( Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
        setContentView(R.layout.create_secgroup);

        requestQueue= Volley.newRequestQueue(getApplicationContext());    
        setBaseURL();
     
 	
		  final EditText name=(EditText)findViewById(R.id.secname);
		  final EditText desc = (EditText) findViewById(R.id.secdesc);
		  Button create= (Button) findViewById(R.id.create);

        
  create.setOnClickListener(new Button.OnClickListener(){
        	
			@Override
			public void onClick(View v) {
				 
				createSecgroup(name.getText().toString(),desc.getText().toString());
			}});
    }

   
	 private void setBaseURL()
		{
		    SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(getApplicationContext());
	        String host_ip = preferences.getString("host_ip","");
	        tenant_id=preferences.getString("tenant_id","" );
	        if(!host_ip.equalsIgnoreCase(""))
	        {
	          ip= host_ip;
	          url_services="http://"+ip+":9696/v2.0";
	        }
	        else
	        {
	      
	       Toast.makeText(getApplicationContext(), "Please configure openstack server", Toast.LENGTH_SHORT).show();
	       Intent intent= new Intent(this,SettingsActivity.class);
	       startActivityForResult(intent,0);
	        }
		}
	 void createSecgroup(String name,String desc)
	 {
		        JsonObjectRequest request = null ;		
				JSONObject json = new JSONObject();
				JSONObject json1 = new JSONObject();
				try{				        
		        // Add a JSON Object
				
		      
		        json.put( "name",name);
		      
		        json.put( "description", desc);
		        json1.put("security_group",json);        
		        // Passing a number to toString() adds indentation
		        System.out.println( json1.toString(2) );
				} catch (JSONException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				URL url;
				try {
					url = new URL(url_services+"/security-groups");
					request= new SetAuthTokenHeader(
				            Request.Method.POST,
				            url.toString().trim(),json1,
				            successListener(),
				            ErrorListener());
					requestQueue.add(request);
				} catch (MalformedURLException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				
			
	            
	 }
	 private Response.Listener<JSONObject> successListener() {
	        return new Response.Listener<JSONObject>() {
	            @Override
	            public void onResponse(JSONObject response) {
	            	//TextView txt=(TextView)getView().findViewById(R.id.label);
	            	System.out.println(response.toString());
	            	//txt.setText(response.toString());
	            	Toast.makeText(getApplicationContext(), "Security Group Created Successfully", Toast.LENGTH_LONG).show();
	            	Intent intent= new Intent(getApplicationContext(),MainActivity.class);
			        startActivityForResult(intent,0);	 	             
	            }
	        };
	    
	    }
	    
	    
	    private Response.ErrorListener ErrorListener() {
	        return new Response.ErrorListener() {
	            @Override
	            public void onErrorResponse(VolleyError error) {
	            	error.printStackTrace();
	            	Toast.makeText(getApplicationContext(), "Security Group Create Failed!", Toast.LENGTH_SHORT).show();
	            }
	        };
	    }
	 
	 
	 
	public class SetAuthTokenHeader extends JsonObjectRequest 
	{
	 public SetAuthTokenHeader(int method, String url, JSONObject jsonRequest,Listener listener, ErrorListener errorListener)
	 {
	   super(method, url, jsonRequest, listener, errorListener);
	 }

	 @Override
	 public Map getHeaders() throws AuthFailureError {
	   Map headers = new HashMap();
	   SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(getApplicationContext());
	   String token = preferences.getString("auth_token","");
	   headers.put("X-Auth-Token", token);
	   return headers;
	 }

	}
	
	} 


