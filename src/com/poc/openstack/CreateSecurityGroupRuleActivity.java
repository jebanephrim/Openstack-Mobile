package com.poc.openstack;


import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.logging.Logger;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import com.android.volley.AuthFailureError;
import com.android.volley.NetworkError;
import com.android.volley.NoConnectionError;
import com.android.volley.ParseError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.ServerError;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.android.volley.Response.ErrorListener;
import com.android.volley.Response.Listener;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;
import com.poc.openstack.ImagesFragment.SetAuthTokenHeader;
import com.poc.openstack.adapter.CustomListViewAdapter;
import com.poc.openstack.adapter.FlavorsSpinnerAdapter;
import com.poc.openstack.adapter.ImagesSpinnerAdapter;
import com.poc.openstack.adapter.KeypairSpinnerAdapter;
import com.poc.openstack.adapter.SecurityRuleSpinnerAdapter;
import com.poc.openstack.model.FlavorsSpinnerItem;
import com.poc.openstack.model.ImagesSpinnerItem;
import com.poc.openstack.model.KeypairSpinnerItem;
import com.poc.openstack.model.SecurityRuleItem;
import com.poc.openstack.model.ServerListItem;

import android.app.Activity;
import android.app.Fragment;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Typeface;
import android.graphics.drawable.Drawable;
import android.os.AsyncTask;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.app.ListFragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.AbsListView;
import android.widget.AbsListView.OnScrollListener;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.AdapterView.OnItemSelectedListener;
import android.widget.ArrayAdapter;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

public class CreateSecurityGroupRuleActivity extends Activity {
	
	public CreateSecurityGroupRuleActivity(){}
	   private static final int RESULTS_PAGE_SIZE = 10;
	   String ip;
	String url_services;
	RequestQueue requestQueue;

	  ListView listView ;
	  String tenant_id;
	  ImagesSpinnerAdapter ispinadapter ;
	  FlavorsSpinnerAdapter fspinadapter;
	  KeypairSpinnerAdapter keyadapter ;
	  SecurityRuleSpinnerAdapter securityadapter;

CustomListViewAdapter adapter;
JSONArray responsejson;

    public void onCreate( Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
        setContentView(R.layout.create_secrule);
       
        final String secgroup_id=getIntent().getStringExtra("secgroup_id");
        Button create= (Button) findViewById(R.id.create);
       final EditText portmax= (EditText) findViewById(R.id.editText2);
       final EditText portmin= (EditText) findViewById(R.id.editText1);
        
        final Spinner dirspinner= (Spinner) findViewById(R.id.direction);
        final Spinner protocolspinner= (Spinner) findViewById(R.id.rule);
        final Spinner etherspinner= (Spinner) findViewById(R.id.ethertype);
        requestQueue= Volley.newRequestQueue(getApplicationContext());    
        setBaseURL();
        List<String> dir = new ArrayList<String>();
    	dir.add("Ingress");
    	dir.add("Egress");
    	List<String> ethertype = new ArrayList<String>();
    	ethertype.add("IPv4");
    	ethertype.add("IPv6");
    	List<String> protocol = new ArrayList<String>();
    	protocol.add("tcp");
    	protocol.add("udp");
    	protocol.add("icmp");
    	ArrayAdapter<String> dirAdapter = new ArrayAdapter<String>(this,
    			android.R.layout.simple_spinner_item, dir);
    	ArrayAdapter<String> etherAdapter = new ArrayAdapter<String>(this,
    			android.R.layout.simple_spinner_item, ethertype);
    	ArrayAdapter<String> protocolAdapter = new ArrayAdapter<String>(this,
    			android.R.layout.simple_spinner_item, protocol);
    	dirAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
    	etherAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
    	protocolAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
    	protocolspinner.setAdapter(protocolAdapter);
    	dirspinner.setAdapter(dirAdapter);
    	etherspinner.setAdapter(etherAdapter);
    	
    	create.setOnClickListener(new OnClickListener(){

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				createsecrule(etherspinner.getSelectedItem().toString(),protocolspinner.getSelectedItem().toString(),dirspinner.getSelectedItem().toString(),portmax.getText().toString(),portmin.getText().toString(),secgroup_id);
			}
        	
        });
        
    }

	 private void setBaseURL()
		{
		    SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(getApplicationContext());
	        String host_ip = preferences.getString("host_ip","");
	        tenant_id=preferences.getString("tenant_id", "");
	        if(!host_ip.equalsIgnoreCase(""))
	        {
	          ip= host_ip;
	          url_services="http://"+ip+":9696/v2.0";
	        }
	        else
	        {
	      
	       Toast.makeText(getApplicationContext(), "Please configure openstack server", Toast.LENGTH_SHORT).show();
	       Intent intent= new Intent(this,SettingsActivity.class);
	       startActivityForResult(intent,0);
	        }
		}
	 void createsecrule(String ethertype, String protocol, String direction, String portmax, String portmin, String secgroup_id)
	 {
		        JsonObjectRequest request = null ;		
				JSONObject json = new JSONObject();
				JSONObject json1 = new JSONObject();
			
				try{				        
		        // Add a JSON Object
				
		   
		        json.put( "ethertype",ethertype);
		        json.put( "protocol", protocol);
		        json.put( "direction", direction);
		        json.put("portmax", portmax);
		        json.put("portmin",portmin);
		        json.put("security_group_id", secgroup_id);
		        json1.put("security_group_rule",json);  
		       
		     
		       
		        // Passing a number to toString() adds indentation
		        System.out.println( json1.toString(2) );
				} catch (JSONException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				URL url;
				try {
					url = new URL(url_services+"/security-group-rules");
					request= new SetAuthTokenHeader(
				            Request.Method.POST,
				            url.toString().trim(),json1,
				            successListener(),
				            ErrorListener());
					requestQueue.add(request);
				} catch (MalformedURLException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				
			
	            
	 }
	 private Response.Listener<JSONObject> successListener() {
	        return new Response.Listener<JSONObject>() {
	            @Override
	            public void onResponse(JSONObject response) {
	            	//TextView txt=(TextView)getView().findViewById(R.id.label);
	            	System.out.println(response.toString());
	            	//txt.setText(response.toString());
	            	Toast.makeText(getApplicationContext(), "Security Group Rule Created Successfully", Toast.LENGTH_LONG).show();
	            	Intent intent= new Intent(getApplicationContext(),MainActivity.class);
			        startActivityForResult(intent,0);	  
	            }
	        };
	    
	    }
	    
	    
	    private Response.ErrorListener ErrorListener() {
	        return new Response.ErrorListener() {
	            @Override
	            public void onErrorResponse(VolleyError error) {
	            	error.printStackTrace();
	            	Toast.makeText(getApplicationContext(), "Create Failed!", Toast.LENGTH_SHORT).show();
	            	if( error instanceof NetworkError) {
	            		Toast.makeText(getApplicationContext(), "Network Error!", Toast.LENGTH_SHORT).show();
					} else if( error instanceof ServerError) {
						
						Toast.makeText(getApplicationContext(), "Server Error!"+error.networkResponse.toString(), Toast.LENGTH_SHORT).show();
					} else if( error instanceof AuthFailureError) {
						Toast.makeText(getApplicationContext(), "Authentication Failed!", Toast.LENGTH_SHORT).show();
					} else if( error instanceof ParseError) {
						Toast.makeText(getApplicationContext(), "Parse Error!", Toast.LENGTH_SHORT).show();
					} else if( error instanceof NoConnectionError) {
						Toast.makeText(getApplicationContext(), "No Connection Error!", Toast.LENGTH_SHORT).show();
					} else if( error instanceof TimeoutError) {
						Toast.makeText(getApplicationContext(), "Timeout Error!", Toast.LENGTH_SHORT).show();
					}
	            }
	        };
	    }
	 
	 

	public class SetAuthTokenHeader extends JsonObjectRequest 
	{
	 public SetAuthTokenHeader(int method, String url, JSONObject jsonRequest,Listener listener, ErrorListener errorListener)
	 {
	   super(method, url, jsonRequest, listener, errorListener);
	 }

	 @Override
	 public Map getHeaders() throws AuthFailureError {
	   Map headers = new HashMap();
	   SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(getApplicationContext());
	   String token = preferences.getString("auth_token","");
	   headers.put("X-Auth-Token", token);
	   return headers;
	 }

	}
	
	} 


