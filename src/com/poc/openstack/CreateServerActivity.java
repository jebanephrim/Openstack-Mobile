package com.poc.openstack;


import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.logging.Logger;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import com.android.volley.AuthFailureError;
import com.android.volley.NetworkError;
import com.android.volley.NoConnectionError;
import com.android.volley.ParseError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.ServerError;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.android.volley.Response.ErrorListener;
import com.android.volley.Response.Listener;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;
import com.poc.openstack.ImagesFragment.SetAuthTokenHeader;
import com.poc.openstack.adapter.CustomListViewAdapter;
import com.poc.openstack.adapter.FlavorsSpinnerAdapter;
import com.poc.openstack.adapter.ImagesSpinnerAdapter;
import com.poc.openstack.adapter.KeypairSpinnerAdapter;
import com.poc.openstack.adapter.NetworkSpinnerAdapter;
import com.poc.openstack.adapter.SecurityRuleSpinnerAdapter;
import com.poc.openstack.model.FlavorsSpinnerItem;
import com.poc.openstack.model.ImagesSpinnerItem;
import com.poc.openstack.model.KeypairSpinnerItem;
import com.poc.openstack.model.NetworkItem;
import com.poc.openstack.model.SecurityRuleItem;
import com.poc.openstack.model.ServerListItem;

import android.app.Activity;
import android.app.Fragment;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Typeface;
import android.graphics.drawable.Drawable;
import android.os.AsyncTask;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.app.ListFragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.AbsListView;
import android.widget.AbsListView.OnScrollListener;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.AdapterView.OnItemSelectedListener;
import android.widget.ArrayAdapter;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

public class CreateServerActivity extends Activity {
	
	public CreateServerActivity(){}
	   private static final int RESULTS_PAGE_SIZE = 10;
	   String ip;
	String url_services;
	RequestQueue requestQueue;
	static int count=0;

	  private ArrayList<ImagesSpinnerItem> mEntries = new ArrayList<ImagesSpinnerItem>();
	  private ArrayList<FlavorsSpinnerItem> fEntries = new ArrayList<FlavorsSpinnerItem>();
	  private ArrayList<KeypairSpinnerItem> keypairEntries = new ArrayList<KeypairSpinnerItem>();
	  private ArrayList<NetworkItem> networkEntries = new ArrayList<NetworkItem>();
	  private ArrayList<SecurityRuleItem> securityruleEntries = new ArrayList<SecurityRuleItem>();
	  ListView listView ;
	  String tenant_id;
	  ImagesSpinnerAdapter ispinadapter ;
	  FlavorsSpinnerAdapter fspinadapter;
	  KeypairSpinnerAdapter keyadapter ;
	  NetworkSpinnerAdapter netadapter ;
	  SecurityRuleSpinnerAdapter securityadapter;

CustomListViewAdapter adapter;
JSONArray responsejson;
private ProgressDialog progress ;

    public void onCreate( Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
        setContentView(R.layout.create_server);
        progress= new ProgressDialog(getApplicationContext());
        tenant_id=getIntent().getStringExtra("tenant_id");
        System.out.println(tenant_id);
        requestQueue= Volley.newRequestQueue(getApplicationContext()); 
        progress.setMessage("Fetching ");
        progress.setProgressStyle(ProgressDialog.STYLE_HORIZONTAL);
        progress.setIndeterminate(true);
        setBaseURL();
        fetchImages();
        getFlavorList();
        getKeyPairs();
        getSecurityRules();
        getNetworks();
         
          final Spinner image=(Spinner)findViewById(R.id.imageref);
		  final Spinner flavor=(Spinner)findViewById(R.id.flavorref);
		  final Spinner access_security= (Spinner) findViewById(R.id.security_rule);
		  final Spinner keypair= (Spinner) findViewById(R.id.keypair);
		  final Spinner netw= (Spinner) findViewById(R.id.spinner1);
		  final EditText instancename=(EditText)findViewById(R.id.editText1);
       
		 
		ispinadapter = new ImagesSpinnerAdapter(this, R.layout.simplespinner, mEntries);
		fspinadapter = new FlavorsSpinnerAdapter(this, R.layout.simplespinner, fEntries);
		keyadapter = new KeypairSpinnerAdapter(this, R.layout.simplespinner, keypairEntries);
		securityadapter = new SecurityRuleSpinnerAdapter(this, R.layout.simplespinner, securityruleEntries);
		netadapter = new NetworkSpinnerAdapter(this, R.layout.simplespinner, networkEntries);
		//spinadapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
		keypair.setAdapter(keyadapter);
        access_security.setAdapter(securityadapter);   
		image.setAdapter(ispinadapter);
	    flavor.setAdapter(fspinadapter);
	    netw.setAdapter(netadapter);
	        
	        
	         
        //View rootView = inflater.inflate(R.layout.create_server, container, false);
    //    TextView tv= (TextView) rootView.findViewById(R.id.loadingServers);
      //  tv.setVisibility(View.VISIBLE);
       // setBaseURL();
      //  fetchInstances();
        Button create= (Button) findViewById(R.id.create);
       // Button view_manage= (Button) rootView.findViewById(R.id.button2);
        
  create.setOnClickListener(new Button.OnClickListener(){
        	
			@Override
			public void onClick(View v) {
				createInstance(instancename.getText().toString(),mEntries.get(image.getSelectedItemPosition()).getImageId(),fEntries.get(flavor.getSelectedItemPosition()).getImageId(),securityruleEntries.get(access_security.getSelectedItemPosition()).getTitle(),networkEntries.get(netw.getSelectedItemPosition()).getNetworkId(),keypairEntries.get(keypair.getSelectedItemPosition()).getTitle());
			}});
        
  
  	
	/*	@Override
		public void onClick(View v) {
			Fragment newFragment = new CreateServerFragment();
			android.app.FragmentTransaction transaction = getFragmentManager().beginTransaction();

			// Replace whatever is in the fragment_container view with this fragment,
			// and add the transaction to the back stack
			transaction.replace(R.id.frame_container, newFragment);
			transaction.addToBackStack(null);

			// Commit the transaction
			transaction.commit();
		}
		});
		*/
	
       // return rootView;
    }
	// @Override 
	  //  public void onActivityCreated(Bundle savedInstanceState) {  
	    //    super.onActivityCreated(savedInstanceState);  
	           
	        //add data to ListView  
	  //    listView = (ListView) getActivity().findViewById(R.id.listofServers);  
 	    //  adapter=new CustomListViewAdapter(getActivity(),0,mEntries);  
	      // listView.setAdapter(adapter);    
	       //listView.setTextFilterEnabled(true);
	    
	       
	           
	//    }  
	 
	 private void setBaseURL()
		{
		    SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(getApplicationContext());
	        String host_ip = preferences.getString("host_ip","");
	        if(!host_ip.equalsIgnoreCase(""))
	        {
	          ip= host_ip;
	          url_services="http://"+ip+":8774/v2";
	        }
	        else
	        {
	      
	       Toast.makeText(getApplicationContext(), "Please configure openstack server", Toast.LENGTH_SHORT).show();
	       Intent intent= new Intent(this,SettingsActivity.class);
	       startActivityForResult(intent,0);
	        }
		}
	 void createInstance(String instancename, String imageId, String flavorId, String security_group, String netid, String key)
	 {
		 JsonObjectRequest request = null ;		
				JSONObject json = new JSONObject();
				JSONObject json1 = new JSONObject();
				JSONObject network= new JSONObject();
				JSONArray securityg= new JSONArray();
				JSONObject securityg1= new JSONObject();
				JSONArray net=new JSONArray();
				try{				        
		        // Add a JSON Object
				
		   
		        json.put( "name",instancename);
		        json.put( "imageRef", imageId);
		        json.put( "flavorRef", flavorId);
		        json.put("key_name", key);
		        securityg1.put("name",security_group);
		        securityg.put(0, securityg1);
		       
		        network.put("uuid", netid );
		        network.put("port","");
		        net.put(0, network);
		        json.put("security_groups",securityg);
		        json.put("networks",net);
		        json1.put("server",json);  
		       
		        // Passing a number to toString() adds indentation
		        System.out.println( json1.toString(2) );
				} catch (JSONException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				URL url;
				try {
					url = new URL(url_services+"/"+tenant_id+"/servers");
					request= new SetAuthTokenHeader(
				            Request.Method.POST,
				            url.toString().trim(),json1,
				            successListener(),
				            ErrorListener());
					requestQueue.add(request);
				} catch (MalformedURLException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				
			
	            
	 }
	 private Response.Listener<JSONObject> successListener() {
	        return new Response.Listener<JSONObject>() {
	            @Override
	            public void onResponse(JSONObject response) {
	            	//TextView txt=(TextView)getView().findViewById(R.id.label);
	            	System.out.println(response.toString());
	            	//txt.setText(response.toString());
	            
	            	Toast.makeText(getApplicationContext(), "Server Created Successfully", Toast.LENGTH_LONG).show();
	            	Intent intent= new Intent(getApplicationContext(),MainActivity.class);
			        startActivityForResult(intent,0);	  
	            }
	        };
	    
	    }
	    
	    
	    private Response.ErrorListener ErrorListener() {
	        return new Response.ErrorListener() {
	            @Override
	            public void onErrorResponse(VolleyError error) {
	            	error.printStackTrace();
	            //	Toast.makeText(getApplicationContext(), "Create Failed!", Toast.LENGTH_SHORT).show();
	            	if( error instanceof NetworkError) {
	            		Toast.makeText(getApplicationContext(), "Network Error!", Toast.LENGTH_SHORT).show();
					} else if( error instanceof ServerError) {
						
						Toast.makeText(getApplicationContext(), "Server Error!"+error.networkResponse.toString(), Toast.LENGTH_SHORT).show();
					} else if( error instanceof AuthFailureError) {
						Toast.makeText(getApplicationContext(), "Authentication Failed!", Toast.LENGTH_SHORT).show();
					} else if( error instanceof ParseError) {
						Toast.makeText(getApplicationContext(), "Parse Error!", Toast.LENGTH_SHORT).show();
					} else if( error instanceof NoConnectionError) {
						Toast.makeText(getApplicationContext(), "No Connection Error!", Toast.LENGTH_SHORT).show();
					} else if( error instanceof TimeoutError) {
						Toast.makeText(getApplicationContext(), "Timeout Error!", Toast.LENGTH_SHORT).show();
					}
	            }
	        };
	    }
	 
	 void fetchImages()
		{
			try {
				//RequestQueue requestQueue = Volley.newRequestQueue(getApplicationContext());    
				int startIndex = 1 + mEntries.size();
				SetAuthTokenHeader request = null ;	
				
				 progress.setMessage("Fetching Images ");
                 progress.setProgressStyle(ProgressDialog.STYLE_HORIZONTAL);
                 progress.setIndeterminate(true);
			
				
					URL url = new URL(url_services+"/"+tenant_id+"/images");
					request= new  SetAuthTokenHeader(
				            Request.Method.GET,
				            url.toString().trim(),null,
				            imagesSuccessListener(),
				            imagesErrorListener());
					
					requestQueue.add(request);
				
		            
		          
				

		        } catch (Exception e) {
		        	
	                e.printStackTrace();
		            
		    
		        }
			
			
		}
	 
	 private Response.Listener<JSONObject> imagesSuccessListener() {
	        return new Response.Listener<JSONObject>() {
	            @Override
	            public void onResponse(JSONObject response) {
	            	//TextView txt=(TextView)getView().findViewById(R.id.label);
	            //	System.out.println(response.toString());
	            	//txt.setText(response.toString());
	            	 String name = null ;
	            	 String id = null;
	            		if(count==5)
		            	{
		            		progress.dismiss();
		            	}
		            	else
		            	{
		            	count=count+1;
		            	}
	            	 
	            	try {
	            		responsejson= response.getJSONArray("images");
	            		JSONObject entry;
	            		 
	            		//System.out.println(responsejson.length());
	                    for (int i = 0; i < responsejson.length(); i++) {
	                    	
	                    	entry = responsejson.getJSONObject(i);  
	                         name = entry.getString("name");
	                       id =entry.getString("id");
	                      
	                   mEntries.add(new ImagesSpinnerItem(name,id));
	                   
	                        }
	                        
	                  
	                    
	                    ispinadapter.notifyDataSetChanged();
					            }
					 catch (JSONException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
	      
	            	               }
	        };
	    
	    }
	    
	    
	    private Response.ErrorListener imagesErrorListener() {
	        return new Response.ErrorListener() {
	            @Override
	            public void onErrorResponse(VolleyError error) {
	            	Toast.makeText(getApplicationContext(), "Fetch Failed!", Toast.LENGTH_SHORT).show();
	            	error.printStackTrace();
	            }
	        };
	    }
	  
	 
	 void getFlavorList()
		{
			try {
			//	RequestQueue requestQueue = Volley.newRequestQueue(getApplicationContext());    

				 JsonObjectRequest flavorDetailsRequest = null ;	
		
					URL url1 = new URL(url_services+"/"+tenant_id+"/flavors");
					 progress.setMessage("Fetching Flavors ");
	                 progress.setProgressStyle(ProgressDialog.STYLE_HORIZONTAL);
	                 progress.setIndeterminate(true);
			
				
				
					flavorDetailsRequest= new  SetAuthTokenHeader(
				            Request.Method.GET,
				            url1.toString().trim(),null,
				            flavorSuccessListener(),
				            flavorErrorListener());
					
					requestQueue.add(flavorDetailsRequest);
		          
				

		        } catch (Exception e) {
		        	
	                e.printStackTrace();
		            
		    
		        }
		}
		
	 private Response.Listener<JSONObject> flavorSuccessListener() {
	        return new Response.Listener<JSONObject>() {
	            @Override
	            public void onResponse(JSONObject response) {
	            
	              // System.out.println(response.toString());
	            	if(count==5)
	            	{
	            		progress.dismiss();
	            	}
	            	else
	            	{
	            	count=count+1;
	            	}
	            		
	            	String name;
	            	String id;
	            	 	try {
		            		responsejson= response.getJSONArray("flavors");
		            		JSONObject entry;
		            		
		            		//System.out.println(responsejson.length());
		                    for (int i = 1; i < responsejson.length(); i++) {
		                        entry = responsejson.getJSONObject(i);  
		                         name = entry.getString("name");
		                       id =entry.getString("id");
		                      
		                   fEntries.add(new FlavorsSpinnerItem(name,id));
		                        }
		                        
		                  
		                    
		                    fspinadapter.notifyDataSetChanged();
	            	
	            	 
						
					} catch (JSONException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
	         
	            	               }
	        };
	    
	    }
	    
	    
	    private Response.ErrorListener flavorErrorListener() {
	        return new Response.ErrorListener() {
	            @Override
	            public void onErrorResponse(VolleyError error) {
	            	error.printStackTrace();
	            	Toast.makeText(getApplicationContext(), "Fetch Failed!", Toast.LENGTH_SHORT).show();
	            }
	        };
	    }
	    
	    void getKeyPairs()
		{
			try {
				//RequestQueue requestQueue = Volley.newRequestQueue(getApplicationContext());    
				int startIndex = 1 + mEntries.size();
				SetAuthTokenHeader request = null ;	
				 progress.setMessage("Fetching Keypairs ");
                 progress.setProgressStyle(ProgressDialog.STYLE_HORIZONTAL);
                 progress.setIndeterminate(true);
				
			
				
					URL url = new URL(url_services+"/"+tenant_id+"/os-keypairs");
					request= new  SetAuthTokenHeader(
				            Request.Method.GET,
				            url.toString().trim(),null,
				           keypairSuccessListener(),
				            keypairErrorListener());
					
					requestQueue.add(request);
				
		            
		          
				

		        } catch (Exception e) {
		        	
	                e.printStackTrace();
		            
		    
		        }
			
			
		}
	 
	 private Response.Listener<JSONObject> keypairSuccessListener() {
	        return new Response.Listener<JSONObject>() {
	            @Override
	            public void onResponse(JSONObject response) {
	            	//TextView txt=(TextView)getView().findViewById(R.id.label);
	             	System.out.println(response.toString());
	            	//txt.setText(response.toString());
	            	 String name = null ;
	            	 String id = null;
	            		if(count==5)
		            	{
		            		progress.dismiss();
		            	}
		            	else
		            	{
		            	count=count+1;
		            	}
	            	 
	            	try {
	            		responsejson= response.getJSONArray("keypairs");
	            		JSONObject entry;
	            		if(responsejson.length()!= 0)
	            		{
	            		//System.out.println(responsejson.length());
	                    for (int i = 0; i < responsejson.length(); i++) {
	                        entry = responsejson.getJSONObject(i);  
	                         name = entry.getJSONObject("keypair").getString("name");
	                       id ="1" ;
	                      
	                   keypairEntries.add(new KeypairSpinnerItem(name,id));
	                        }
	                        
	                    keyadapter.notifyDataSetChanged();
	            		}
	            		else
	            		{
	            			keypairEntries.add(new KeypairSpinnerItem("Empty","0"));
	            			 keyadapter.notifyDataSetChanged();
	            		}
	                   
					            }
					 catch (JSONException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
	            	
	            	 
	         			
			         
	         			
	       // Intent intent= new Intent(getActivity().getApplicationContext(),MainActivity.class);
					//startActivityForResult(intent,0);
	         
	            	               }
	        };
	    
	    }
	    
	    
	    private Response.ErrorListener keypairErrorListener() {
	        return new Response.ErrorListener() {
	            @Override
	            public void onErrorResponse(VolleyError error) {
	            	Toast.makeText(getApplicationContext(), "Fetch Failed!", Toast.LENGTH_SHORT).show();
	            	error.printStackTrace();
	            }
	        };
	    }
	  
	    void getSecurityRules()
		{
			try {
				//RequestQueue requestQueue = Volley.newRequestQueue(getApplicationContext());    
				//int startIndex = 1 + mEntries.size();
				SetAuthTokenHeader request = null ;					
				 progress.setMessage("Fetching Security Groups");
                 progress.setProgressStyle(ProgressDialog.STYLE_HORIZONTAL);
                 progress.setIndeterminate(true);
					URL url = new URL(url_services+"/"+tenant_id+"/os-security-groups");
					request= new  SetAuthTokenHeader(
				            Request.Method.GET,
				            url.toString().trim(),null,
				            securitySuccessListener(),
				            securityErrorListener());
					
					requestQueue.add(request);
		        } catch (Exception e) {
		        	
	                e.printStackTrace();
		            
		    
		        }
			
			
		}
	 
	 private Response.Listener<JSONObject> securitySuccessListener() {
	        return new Response.Listener<JSONObject>() {
	            @Override
	            public void onResponse(JSONObject response) {
	            	//TextView txt=(TextView)getView().findViewById(R.id.label);
	             	//System.out.println(response.toString());
	            	//txt.setText(response.toString());
	            	 String name = null ;
	            	 String id = null;
	           
	            		if(count==5)
		            	{
		            		progress.dismiss();
		            	}
		            	else
		            	{
		            	count=count+1;
		            	}
	            	try {
	            		responsejson= response.getJSONArray("security_groups");
	            		JSONObject entry;
	            		//System.out.println(responsejson.length());
	            		 
	                    for (int i = 0; i < responsejson.length(); i++) {
	                    	
	                        entry = responsejson.getJSONObject(i);  
	                         name = entry.getString("name");
	                       id =entry.getString("id");
	                      
	                   securityruleEntries.add(new SecurityRuleItem(name,id));
	                       
	                    }
	                        
	                    securityadapter.notifyDataSetChanged();
	                 
	                  
					            }
					 catch (JSONException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
	 
	            	               }
	        };
	    
	    }
	    
	    
	    private Response.ErrorListener securityErrorListener() {
	        return new Response.ErrorListener() {
	            @Override
	            public void onErrorResponse(VolleyError error) {
	            	 progress.dismiss();
	            	Toast.makeText(getApplicationContext(), " Security Rule Fetch Failed!", Toast.LENGTH_SHORT).show();
	            	error.printStackTrace();
	            }
	        };
	    }
	  
	 
	    void getNetworks()
		{
			try {
				//RequestQueue requestQueue = Volley.newRequestQueue(getApplicationContext());    
				//int startIndex = 1 + mEntries.size();
				SetAuthTokenHeader request = null ;		
				 progress.setMessage("Fetching Networks ");
                 progress.setProgressStyle(ProgressDialog.STYLE_HORIZONTAL);
                 progress.setIndeterminate(true);
				URL url = new URL("http://"+ip+":9696/v2.0/networks");
					request= new  SetAuthTokenHeader(
				            Request.Method.GET,
				            url.toString().trim(),null,
				            netSuccessListener(),
				            netErrorListener());
					
					requestQueue.add(request);
		        } catch (Exception e) {
		        	
	                e.printStackTrace();
		            
		    
		        }
			
			
		}
	 
	 private Response.Listener<JSONObject> netSuccessListener() {
	        return new Response.Listener<JSONObject>() {
	            @Override
	            public void onResponse(JSONObject response) {
	            	//TextView txt=(TextView)getView().findViewById(R.id.label);
	             	//System.out.println(response.toString());
	            	//txt.setText(response.toString());
	            	 String name = null ;
	            	 String id = null;
	            		if(count==4)
		            	{
		            		progress.dismiss();
		            	}
		            	else
		            	{
		            	count=count+1;
		            	}
	            	 
	            	try {
	            		responsejson= response.getJSONArray("networks");
	            		JSONObject entry;
	            		//System.out.println(responsejson.length());
	            		 
	                    for (int i = 0; i < responsejson.length(); i++) {
	                    	
	                        entry = responsejson.getJSONObject(i);  
	                         name = entry.getString("name");
	                       id =entry.getString("id");
	                      
	                   networkEntries.add(new NetworkItem(name,id));
	                       
	                    }
	                        
	                    netadapter.notifyDataSetChanged();
	                 
	                  
					            }
					 catch (JSONException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
	 
	            	               }
	        };
	    
	    }
	    
	    
	    private Response.ErrorListener netErrorListener() {
	        return new Response.ErrorListener() {
	            @Override
	            public void onErrorResponse(VolleyError error) {
	            	 progress.setMessage("Fetch failed. Try Again ");
	                 progress.setProgressStyle(ProgressDialog.STYLE_HORIZONTAL);
	                 progress.setIndeterminate(true);
	            	Toast.makeText(getApplicationContext(), " Networks Fetch Failed!", Toast.LENGTH_SHORT).show();
	            	error.printStackTrace();
	            }
	        };
	    }
	  
	    

	public class SetAuthTokenHeader extends JsonObjectRequest 
	{
	 public SetAuthTokenHeader(int method, String url, JSONObject jsonRequest,Listener listener, ErrorListener errorListener)
	 {
	   super(method, url, jsonRequest, listener, errorListener);
	 }

	 @Override
	 public Map getHeaders() throws AuthFailureError {
	   Map headers = new HashMap();
	   SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(getApplicationContext());
	   String token = preferences.getString("auth_token","");
	   headers.put("X-Auth-Token", token);
	   return headers;
	 }

	}
	
	} 


