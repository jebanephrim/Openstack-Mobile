package com.poc.openstack;



import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.logging.Logger;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.Response.ErrorListener;
import com.android.volley.Response.Listener;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;
import com.poc.openstack.ImagesFragment.SetAuthTokenHeader;
import com.poc.openstack.adapter.CustomListViewAdapter;
import com.poc.openstack.adapter.FlavorsSpinnerAdapter;
import com.poc.openstack.adapter.ImagesSpinnerAdapter;
import com.poc.openstack.model.FlavorsSpinnerItem;
import com.poc.openstack.model.ImagesSpinnerItem;
import com.poc.openstack.model.ServerListItem;

import android.app.Activity;
import android.app.Fragment;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Typeface;
import android.graphics.drawable.Drawable;
import android.os.AsyncTask;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.app.ListFragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.AbsListView;
import android.widget.AbsListView.OnScrollListener;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.AdapterView.OnItemSelectedListener;
import android.widget.ArrayAdapter;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

public class CreateUserActivity extends Activity {
	
	public CreateUserActivity(){}
	   private static final int RESULTS_PAGE_SIZE = 10;
	   String ip;
	String url_services;
	RequestQueue requestQueue;
	 Button create;
	Boolean isenabled;
	  private ArrayList<ImagesSpinnerItem> mEntries = new ArrayList<ImagesSpinnerItem>();
	  private ArrayList<FlavorsSpinnerItem> fEntries = new ArrayList<FlavorsSpinnerItem>();
	  private ArrayList<ServerListItem> mEntries1 = new ArrayList<ServerListItem>();
	  ListView listView ;
	  String tenant_id;
	  ImagesSpinnerAdapter ispinadapter ;
	  FlavorsSpinnerAdapter fspinadapter;

CustomListViewAdapter adapter;
JSONArray responsejson;

    public void onCreate( Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
        setContentView(R.layout.createuser);

        requestQueue= Volley.newRequestQueue(getApplicationContext());    
        setBaseURL();
     
 	
		  final EditText user_name=(EditText)findViewById(R.id.uname);
		  final EditText user_desc=(EditText)findViewById(R.id.udesc);
		  final EditText email= (EditText)findViewById(R.id.email);
		  create= (Button) findViewById(R.id.createUser);
		  final CheckBox check = (CheckBox)findViewById(R.id.checkBox1);
		
        //View rootView = inflater.inflate(R.layout.create_server, container, false);
    //    TextView tv= (TextView) rootView.findViewById(R.id.loadingServers);
      //  tv.setVisibility(View.VISIBLE);
       // setBaseURL();
      //  fetchInstances();
       
       // Button view_manage= (Button) rootView.findViewById(R.id.button2);
        
  create.setOnClickListener(new Button.OnClickListener(){
        	
			@Override
			public void onClick(View v) {
				 if (check.isChecked()) {
                     isenabled=true;
                 }
                 else
                 	isenabled=false;
				 create.setClickable(false);
				createUser(user_name.getText().toString(),user_desc.getText().toString(),email.getText().toString(),isenabled);
			}});
        
  
  	
	/*	@Override
		public void onClick(View v) {
			Fragment newFragment = new CreateServerFragment();
			android.app.FragmentTransaction transaction = getFragmentManager().beginTransaction();

			// Replace whatever is in the fragment_container view with this fragment,
			// and add the transaction to the back stack
			transaction.replace(R.id.frame_container, newFragment);
			transaction.addToBackStack(null);

			// Commit the transaction
			transaction.commit();
		}
		});
		*/
	
       // return rootView;
    }
	// @Override 
	  //  public void onActivityCreated(Bundle savedInstanceState) {  
	    //    super.onActivityCreated(savedInstanceState);  
	           
	        //add data to ListView  
	  //    listView = (ListView) getActivity().findViewById(R.id.listofServers);  
 	    //  adapter=new CustomListViewAdapter(getActivity(),0,mEntries);  
	      // listView.setAdapter(adapter);    
	       //listView.setTextFilterEnabled(true);
	    
	       
	           
	//    }  
	 
	 private void setBaseURL()
		{
		    SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(getApplicationContext());
	        String host_ip = preferences.getString("host_ip","");
	        if(!host_ip.equalsIgnoreCase(""))
	        {
	          ip= host_ip;
	          url_services="http://"+ip+":5000/v3";
	        }
	        else
	        {
	      
	       Toast.makeText(getApplicationContext(), "Please configure openstack server", Toast.LENGTH_SHORT).show();
	       Intent intent= new Intent(this,SettingsActivity.class);
	       startActivityForResult(intent,0);
	        }
		}
	 void createUser(String name, String desc, String email,Boolean isenabled)
	 {
		        JsonObjectRequest request = null ;		
				JSONObject json = new JSONObject();
				JSONObject json1 = new JSONObject();
				try{				        
		        // Add a JSON Object
				
		   
		        json.put( "name",name);
		       // json.put( "description", desc);
		        json.put("email", email);
		        json.put( "enabled", isenabled);
		        json.put("password", "12345");
		       // json.put("default_project_id", "4677e82bcda0464c9424649f74127d5f");
		        json1.put("user",json);        
		        // Passing a number to toString() adds indentation
		        System.out.println( json1.toString(2) );
				} catch (JSONException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				URL url;
				try {
					url = new URL(url_services+"/users");
					request= new SetAuthTokenHeader(
				            Request.Method.POST,
				            url.toString().trim(),json1,
				            successListener(),
				            ErrorListener());
					requestQueue.add(request);
				} catch (MalformedURLException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				
			
	            
	 }
	 private Response.Listener<JSONObject> successListener() {
	        return new Response.Listener<JSONObject>() {
	            @Override
	            public void onResponse(JSONObject response) {
	            	//TextView txt=(TextView)getView().findViewById(R.id.label);
	            	//System.out.println(response.toString());
	            	//txt.setText(response.toString());
	            	try {
						Toast.makeText(getApplicationContext(), "Created User: "+response.getJSONObject("user").getString("name"), Toast.LENGTH_LONG).show();
						Intent intent= new Intent(getApplicationContext(),MainActivity.class);
				        startActivityForResult(intent,0);	 
	            	} catch (JSONException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
	            	               }
	        };
	    
	    }
	    
	    
	    private Response.ErrorListener ErrorListener() {
	        return new Response.ErrorListener() {
	            @Override
	            public void onErrorResponse(VolleyError error) {
	            	error.printStackTrace();
	            	
	            	Toast.makeText(getApplicationContext(), "Create User Failed!", Toast.LENGTH_SHORT).show();
	                create.setClickable(true);
	            }
	        };
	    }
	 
	 
	 
	public class SetAuthTokenHeader extends JsonObjectRequest 
	{
	 public SetAuthTokenHeader(int method, String url, JSONObject jsonRequest,Listener listener, ErrorListener errorListener)
	 {
	   super(method, url, jsonRequest, listener, errorListener);
	 }

	 @Override
	 public Map getHeaders() throws AuthFailureError {
	   Map headers = new HashMap();
	   SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(getApplicationContext());
	   String token = preferences.getString("auth_token","");
	   headers.put("X-Auth-Token", token);
	   return headers;
	 }

	}
	
	} 


