package com.poc.openstack;



import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.logging.Logger;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.Response.ErrorListener;
import com.android.volley.Response.Listener;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;
import com.poc.openstack.ImagesFragment.SetAuthTokenHeader;
import com.poc.openstack.adapter.CustomListViewAdapter;
import com.poc.openstack.adapter.FlavorsSpinnerAdapter;
import com.poc.openstack.adapter.ImagesSpinnerAdapter;
import com.poc.openstack.model.FlavorsSpinnerItem;
import com.poc.openstack.model.ImagesSpinnerItem;
import com.poc.openstack.model.ServerListItem;

import android.app.Activity;
import android.app.Fragment;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Typeface;
import android.graphics.drawable.Drawable;
import android.os.AsyncTask;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.app.ListFragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.AbsListView;
import android.widget.AbsListView.OnScrollListener;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.AdapterView.OnItemSelectedListener;
import android.widget.ArrayAdapter;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.CompoundButton.OnCheckedChangeListener;

import android.widget.EditText;
import android.widget.ListView;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

public class CreateVolumeActivity extends Activity {
	
	public CreateVolumeActivity(){}
	   private static final int RESULTS_PAGE_SIZE = 10;
	   String ip;
	String url_services;
	RequestQueue requestQueue;
	Boolean isenabled=false;
	  private ArrayList<ImagesSpinnerItem> mEntries = new ArrayList<ImagesSpinnerItem>();

	  ListView listView ;
	  String tenant_id;
	  ImagesSpinnerAdapter ispinadapter ;
	
CustomListViewAdapter adapter;
JSONArray responsejson;

    public void onCreate( Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
        setContentView(R.layout.create_volume);
        tenant_id=getIntent().getStringExtra("tenant_id");
        requestQueue= Volley.newRequestQueue(getApplicationContext());    
        setBaseURL();
        fetchImages();
 	
		  final EditText name=(EditText)findViewById(R.id.vname);
		  final EditText desc=(EditText)findViewById(R.id.vdesc);
		  final EditText size= (EditText)findViewById(R.id.size);
		  final Spinner image= (Spinner)findViewById(R.id.spinner1);
		  Button create= (Button) findViewById(R.id.vcreate);
		  final CheckBox check = (CheckBox)findViewById(R.id.checkBox1);
		  ispinadapter = new ImagesSpinnerAdapter(this, R.layout.simplespinner, mEntries);
		  image.setAdapter(ispinadapter);
		  check.setOnCheckedChangeListener(new OnCheckedChangeListener(){
				  
					@Override
					public void onCheckedChanged(CompoundButton buttonView,
							boolean isChecked) {
						// TODO Auto-generated method stub
						if (isChecked) {
							 image.setVisibility(View.VISIBLE);
							 isenabled=true;
			             
			          }
			          else
			          {
			        	  image.setVisibility(View.GONE);
					      isenabled=false;
			          }
					}
					  
				  });
		 
		
  create.setOnClickListener(new Button.OnClickListener(){
        	
			@Override
			public void onClick(View v) {
				if(isenabled)
				createVolume(name.getText().toString(),desc.getText().toString(),size.getText().toString(),mEntries.get(image.getSelectedItemPosition()).getImageId());
				else
					createVolume(name.getText().toString(),desc.getText().toString(),size.getText().toString(),null);
			}});
        
  
  	
	/*	@Override
		public void onClick(View v) {
			Fragment newFragment = new CreateServerFragment();
			android.app.FragmentTransaction transaction = getFragmentManager().beginTransaction();

			// Replace whatever is in the fragment_container view with this fragment,
			// and add the transaction to the back stack
			transaction.replace(R.id.frame_container, newFragment);
			transaction.addToBackStack(null);

			// Commit the transaction
			transaction.commit();
		}
		});
		*/
	
       // return rootView;
    }
	// @Override 
	  //  public void onActivityCreated(Bundle savedInstanceState) {  
	    //    super.onActivityCreated(savedInstanceState);  
	           
	        //add data to ListView  
	  //    listView = (ListView) getActivity().findViewById(R.id.listofServers);  
 	    //  adapter=new CustomListViewAdapter(getActivity(),0,mEntries);  
	      // listView.setAdapter(adapter);    
	       //listView.setTextFilterEnabled(true);
	    
	       
	           
	//    }  
	 
	 private void setBaseURL()
		{
		    SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(getApplicationContext());
	        String host_ip = preferences.getString("host_ip","");
	        if(!host_ip.equalsIgnoreCase(""))
	        {
	          ip= host_ip;
	          url_services="http://"+ip+":8776/v2";
	        }
	        else
	        {
	      
	       Toast.makeText(getApplicationContext(), "Please configure openstack server", Toast.LENGTH_SHORT).show();
	       Intent intent= new Intent(this,SettingsActivity.class);
	       startActivityForResult(intent,0);
	        }
		}
	 void createVolume(String name, String desc, String size,String imageref)
	 {
		        JsonObjectRequest request = null ;		
				JSONObject json = new JSONObject();
				JSONObject json1 = new JSONObject();
				try{				        
		        // Add a JSON Object
				/*	{
					    "volume": {
					        "availability_zone": null,
					        "source_volid": null,
					        "display_description": null,
					        "snapshot_id": null,
					        "size": 10,
					        "display_name": "my_volume",
					        "imageRef": null,
					        "volume_type": null,
					        "metadata": {}
					    }
					}*/
		   
		        json.put( "name",name);
		     
		        json.put("size", size);
		        json.put( "display_description", desc);
		        json.put("imageRef", imageref);
		        json1.put("volume",json);        
		        // Passing a number to toString() adds indentation
		        System.out.println( json1.toString(2) );
				} catch (JSONException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				URL url;
				try {
					url = new URL(url_services+"/"+tenant_id+"/volumes");
					request= new SetAuthTokenHeader(
				            Request.Method.POST,
				            url.toString().trim(),json1,
				            successListener(),
				            ErrorListener());
					requestQueue.add(request);
				} catch (MalformedURLException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				
			
	            
	 }
	 private Response.Listener<JSONObject> successListener() {
	        return new Response.Listener<JSONObject>() {
	            @Override
	            public void onResponse(JSONObject response) {
	            	//TextView txt=(TextView)getView().findViewById(R.id.label);
	            	System.out.println(response.toString());
	            	//txt.setText(response.toString());
	            	try {
						Toast.makeText(getApplicationContext(), "Created Volume "+response.getJSONObject("volume").getString("name"), Toast.LENGTH_LONG).show();
						Intent intent= new Intent(getApplicationContext(),MainActivity.class);
				        startActivityForResult(intent,0);

	            	} catch (JSONException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
	            	               }
	        };
	    
	    }
	    
	    
	    private Response.ErrorListener ErrorListener() {
	        return new Response.ErrorListener() {
	            @Override
	            public void onErrorResponse(VolleyError error) {
	            	error.printStackTrace();
	            	
	            	Toast.makeText(getApplicationContext(), "Create Volume Failed!", Toast.LENGTH_SHORT).show();
	            }
	        };
	    }
	 
	 
	    void fetchImages()
		{
			try {
				//RequestQueue requestQueue = Volley.newRequestQueue(getApplicationContext());    
			//	int startIndex = 1 + mEntries.size();
				SetAuthTokenHeader request = null ;	
				String iurl="http://"+ip+":8774/v2";
				
			
				
					URL url = new URL(iurl+"/"+tenant_id+"/images");
					request= new  SetAuthTokenHeader(
				            Request.Method.GET,
				            url.toString().trim(),null,
				            imagesSuccessListener(),
				            imagesErrorListener());
					
					requestQueue.add(request);
				
		            
		          
				

		        } catch (Exception e) {
		        	
	                e.printStackTrace();
		            
		    
		        }
			
			
		}
	 
	 private Response.Listener<JSONObject> imagesSuccessListener() {
	        return new Response.Listener<JSONObject>() {
	            @Override
	            public void onResponse(JSONObject response) {
	          
	            	 String name = null ;
	            	 String id = null;
	            
	            	 
	            	try {
	            		responsejson= response.getJSONArray("images");
	            		JSONObject entry;
	            		 
	            		//System.out.println(responsejson.length());
	                    for (int i = 0; i < responsejson.length(); i++) {
	                    	
	                    	entry = responsejson.getJSONObject(i);  
	                         name = entry.getString("name");
	                       id =entry.getString("id");
	                      
	                   mEntries.add(new ImagesSpinnerItem(name,id));
	                   
	                        }
	                        
	                  
	                    
	                    ispinadapter.notifyDataSetChanged();
					            }
					 catch (JSONException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
	      
	            	               }
	        };
	    
	    }
	    
	    
	    private Response.ErrorListener imagesErrorListener() {
	        return new Response.ErrorListener() {
	            @Override
	            public void onErrorResponse(VolleyError error) {
	            	Toast.makeText(getApplicationContext(), "Fetch Failed!", Toast.LENGTH_SHORT).show();
	            	error.printStackTrace();
	            }
	        };
	    }
	  
	    
	public class SetAuthTokenHeader extends JsonObjectRequest 
	{
	 public SetAuthTokenHeader(int method, String url, JSONObject jsonRequest,Listener listener, ErrorListener errorListener)
	 {
	   super(method, url, jsonRequest, listener, errorListener);
	 }

	 @Override
	 public Map getHeaders() throws AuthFailureError {
	   Map headers = new HashMap();
	   SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(getApplicationContext());
	   String token = preferences.getString("auth_token","");
	   headers.put("X-Auth-Token", token);
	   return headers;
	 }

	}
	
	} 


