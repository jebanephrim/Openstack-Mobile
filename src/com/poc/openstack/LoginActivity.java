package com.poc.openstack;

import java.net.MalformedURLException;
import java.net.URL;
import java.util.HashMap;
import java.util.Map;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import com.android.volley.AuthFailureError;
import com.android.volley.NetworkResponse;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.Response.ErrorListener;
import com.android.volley.Response.Listener;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;

import android.app.Activity;
import android.app.Fragment;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

public class LoginActivity extends Activity {
	
	public LoginActivity(){}
	ProgressDialog dialog =null;
	ProgressDialog dialog1 =null;
    String ip;
    String username;
	String url_services;
	RequestQueue requestQueue;
	 GetStatusCodeResponse tokenvalidaterequest = null ;
	
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setBaseURL();
		
		setContentView(R.layout.login_page);
       
        final EditText username=(EditText)findViewById(R.id.username);
        final EditText password=(EditText)findViewById(R.id.password);
        final EditText tenantname=(EditText)findViewById(R.id.tenantname);
        Button login=(Button)findViewById(R.id.button1);
       
        
        
        login.setOnClickListener(new Button.OnClickListener(){
        	
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
			    dialog = new ProgressDialog(LoginActivity.this);
                dialog.setMessage("Authenticating..");
                dialog.setCancelable(false);
                dialog.show();
				login(username.getText().toString(),password.getText().toString(),tenantname.getText().toString());
			}});
        
    }
	

	private void setBaseURL()
	{
	    SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(getApplicationContext());
        String host_ip = preferences.getString("host_ip","");
        if(!host_ip.equalsIgnoreCase(""))
        {
          ip= host_ip;
          url_services="http://"+ip+":5000/v2.0/tokens";
        }
        else
        {
      
       Toast.makeText(getApplicationContext(), "Please configure openstack server", Toast.LENGTH_SHORT).show();
       Intent intent= new Intent(this,SettingsActivity.class);
       startActivityForResult(intent,0);
        }
	}
	
	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
	    // Inflate the menu items for use in the action bar
	    MenuInflater inflater = getMenuInflater();
	    inflater.inflate(R.menu.settingsmenu, menu);
	    return super.onCreateOptionsMenu(menu);
	}
	
	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
	    // Handle presses on the action bar items
	    switch (item.getItemId()) {
	     
	        case R.id.action_settings:
	            openSettings();
	            return true;
	        default:
	            return super.onOptionsItemSelected(item);
	    }
	}
	void openSettings()
	{
		Intent myintent= new Intent(getApplicationContext(),SettingsActivity.class);
		startActivityForResult(myintent,0);
	}
	
   void login (String uname, String pass, String tenantname)
   {
	   try {
			requestQueue = Volley.newRequestQueue(getApplicationContext());    
			 JsonObjectRequest request = null ;	
				JSONObject auth = new JSONObject();
				
				 JSONObject password = new JSONObject();
				 JSONObject json = new JSONObject();
				try{				        
		 
				       auth.put("tenantName",tenantname);
				       password.put("username",uname);
				       password.put("password",pass);
				       auth.put("passwordCredentials",password);
				       json.put("auth", auth);
				       
				       
				       
		        // Passing a number to toString() adds indentation
		        System.out.println( json.toString(2) );
				} catch (JSONException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				URL url = new URL(url_services);
				request= new  JsonObjectRequest(
			            Request.Method.POST,
			            url.toString().trim(),json,
			            successListener(),
			            ErrorListener());
				
				requestQueue.add(request);
			
	            
	          
			

	        } catch (Exception e) {
	        	
               e.printStackTrace();
	            
	    
	        }
		
		
	   
	}
	private Response.Listener<JSONObject> successListener() {
       return new Response.Listener<JSONObject>() {
           @Override
           public void onResponse(JSONObject response) {
           	dialog.dismiss();
             String isadmin="false";
           	try {
           		String token=response.getJSONObject("access").getJSONObject("token").getString("id");
           	 	String tenantId=response.getJSONObject("access").getJSONObject("token").getJSONObject("tenant").getString("id");
           		String name=response.getJSONObject("access").getJSONObject("user").getString("username");
           		JSONArray roles=response.getJSONObject("access").getJSONObject("user").getJSONArray("roles");
           		
           	 	System.out.println(roles.toString());
           	 for(int i=0;i<roles.length();i++)
           	 {
           		 if(roles.getJSONObject(i).getString("name").equalsIgnoreCase("admin"))
           		 {
           			System.out.println(roles.getJSONObject(i).getString("name"));
           			 isadmin="true";
           		 }
           	 }
					SharedPreferences setpreferences = PreferenceManager.getDefaultSharedPreferences(getApplicationContext());
        	        SharedPreferences.Editor editor = setpreferences.edit();
        	        editor.putString("uname", name);
        	        editor.putString("auth_token",token);
        	        editor.putString("tenant_id", tenantId);
        	       editor.putString("isAdmin",isadmin);
        	        editor.commit();
        	        Intent intent= new Intent(getApplicationContext(),MainActivity.class);
        	      
        	        startActivityForResult(intent,0);

                   	Toast.makeText(getApplicationContext(), "Welcome "+name+" !", Toast.LENGTH_LONG).show();
                 
				} catch (JSONException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
        
           	               }
       };
   
   }
   
   
   private Response.ErrorListener ErrorListener() {
       return new Response.ErrorListener() {
           @Override
           public void onErrorResponse(VolleyError error) {
            dialog.dismiss();
           	error.printStackTrace();
           	Toast.makeText(getApplicationContext(), "Login Failed!", Toast.LENGTH_SHORT).show();
           }
       };
   }

   public class GetStatusCodeResponse extends JsonObjectRequest 
	{
		int mStatusCode;
	 public GetStatusCodeResponse(int method, String url, JSONObject jsonRequest,Listener listener, ErrorListener errorListener)
	 {
	   super(method, url, jsonRequest, listener, errorListener);
	 }
	 @Override
	 public Map getHeaders() throws AuthFailureError {
	   Map headers = new HashMap();
	   SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(getApplicationContext());
	   String token = preferences.getString("auth_token","");
	   headers.put("X-Auth-Token", token);
	   return headers;
	 }
	 
	 public int getStatusCode() {
	        
			return mStatusCode;
	    }

	    @Override
	    protected Response<JSONObject> parseNetworkResponse(NetworkResponse response) {
	        mStatusCode = response.statusCode;
	        return super.parseNetworkResponse(response);
	    }

	}
	

	

}
