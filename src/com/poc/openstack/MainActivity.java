package com.poc.openstack;


import java.util.ArrayList;

import org.json.JSONArray;
import org.json.JSONException;

import com.poc.openstack.adapter.NavDrawerListAdapter;
import com.poc.openstack.adapter.NewAdapter;
import com.poc.openstack.model.NavDrawerItem;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Fragment;
import android.app.FragmentManager;
import android.app.SearchManager;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.res.Configuration;
import android.content.res.TypedArray;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.v4.app.ActionBarDrawerToggle;
import android.support.v4.app.FragmentActivity;
import android.support.v4.widget.DrawerLayout;
import android.text.TextUtils;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ExpandableListView;
import android.widget.ListView;
import android.widget.SearchView;
import android.widget.SearchView.OnQueryTextListener;

public class MainActivity extends FragmentActivity {
	private DrawerLayout mDrawerLayout;
	//private ListView mDrawerList;
	private ActionBarDrawerToggle mDrawerToggle;
	AlertDialog createDialog ;
	AlertDialog deleteDialog ;
	// nav drawer title
	private CharSequence mDrawerTitle;
	private ListView mDrawerList;
	// used to store app title
	private CharSequence mTitle;
	ArrayList<Object> childItem = new ArrayList<Object>();
	ArrayList<String> groupItem = new ArrayList<String>();
	// slide menu items
	private String[] navMenuTitles;
	private TypedArray navMenuIcons;
	Fragment myFragment;
	private ArrayList<NavDrawerItem> navDrawerItems;
	//private NavDrawerListAdapter adapter;
	private NavDrawerListAdapter adapter;
	Intent intent = getIntent();
    String roles;
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);

		myFragment = (Fragment)getFragmentManager().findFragmentByTag("1");

		mTitle = mDrawerTitle = getTitle();

		// load slide menu items
		navMenuTitles = getResources().getStringArray(R.array.nav_drawer_items);

		// nav drawer icons from resources
		navMenuIcons = getResources()
				.obtainTypedArray(R.array.nav_drawer_icons);
		mDrawerLayout = (DrawerLayout) findViewById(R.id.drawer_layout);
		mDrawerList = (ListView) findViewById(R.id.list_slidermenu);

		navDrawerItems = new ArrayList<NavDrawerItem>();

		// adding nav drawer items to array
		// Home
		navDrawerItems.add(new NavDrawerItem(navMenuTitles[0], navMenuIcons.getResourceId(0, -1)));
		// Find People
		navDrawerItems.add(new NavDrawerItem(navMenuTitles[1], navMenuIcons.getResourceId(1, -1)));
		// Photos
		navDrawerItems.add(new NavDrawerItem(navMenuTitles[2], navMenuIcons.getResourceId(2, -1)));
		// Communities, Will add a counter here
		navDrawerItems.add(new NavDrawerItem(navMenuTitles[3], navMenuIcons.getResourceId(3, -1)));
		// Pages
		navDrawerItems.add(new NavDrawerItem(navMenuTitles[4], navMenuIcons.getResourceId(4, -1)));
		// What's hot, We  will add a counter here
		navDrawerItems.add(new NavDrawerItem(navMenuTitles[5], navMenuIcons.getResourceId(5, -1)));
		navDrawerItems.add(new NavDrawerItem(navMenuTitles[6], navMenuIcons.getResourceId(6, -1)));
	//	navDrawerItems.add(new NavDrawerItem(navMenuTitles[7], navMenuIcons.getResourceId(7, -1)));
		

		// Recycle the typed array
		navMenuIcons.recycle();

		mDrawerList.setOnItemClickListener(new SlideMenuClickListener());

		// setting the nav drawer list adapter
		//adapter = new NavDrawerListAdapter(getApplicationContext(),
			//	navDrawerItems);
		adapter = new NavDrawerListAdapter(getApplicationContext(),
				navDrawerItems);
		mDrawerList.setAdapter(adapter);
	//	mDrawerList.setChoiceMode(ExpandableListView.CHOICE_MODE_SINGLE);
		// enabling action bar app icon and behaving it as toggle button
		getActionBar().setDisplayHomeAsUpEnabled(true);
		getActionBar().setHomeButtonEnabled(true);

		mDrawerToggle = new ActionBarDrawerToggle(this, mDrawerLayout,
				R.drawable.ic_drawer, //nav menu toggle icon
				R.string.app_name, // nav drawer open - description for accessibility
				R.string.app_name // nav drawer close - description for accessibility
		) {
			public void onDrawerClosed(View view) {
				getActionBar().setTitle(mTitle);
				// calling onPrepareOptionsMenu() to show action bar icons
				invalidateOptionsMenu();
			}

			public void onDrawerOpened(View drawerView) {
				getActionBar().setTitle(mDrawerTitle);
				// calling onPrepareOptionsMenu() to hide action bar icons
				invalidateOptionsMenu();
			}
		};
		mDrawerLayout.setDrawerListener(mDrawerToggle);

		if (savedInstanceState == null) {
			// on first time display view for first nav item
			displayView(0);
		}
	}

	/**
	 * Slide menu item click listener
	 * */
	private class SlideMenuClickListener implements
			ListView.OnItemClickListener {
		@Override
		public void onItemClick(AdapterView<?> parent, View view, int position,
				long id) {
			// display view for selected nav drawer item
			displayView(position);
		}
		
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		getMenuInflater().inflate(R.menu.main, menu);
	
	
	       
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// toggle nav drawer on selecting action bar app icon/title
		if (mDrawerToggle.onOptionsItemSelected(item)) {
			return true;
		}
		// Handle action bar actions click
		switch (item.getItemId()) {
		case R.id.settings:
			 Intent intent= new Intent(getApplicationContext(),SettingsActivity.class);
	 	        startActivityForResult(intent,0);
			return true;
			
		 case R.id.signout:
			SharedPreferences setpreferences = PreferenceManager.getDefaultSharedPreferences(getApplicationContext());
 	        SharedPreferences.Editor editor = setpreferences.edit();
 	        editor.putString("auth_token","");
 	        editor.putString("tenant_id", "");
 	        editor.commit();
 	        Intent intent1= new Intent(getApplicationContext(),LoginActivity.class);
 	        startActivityForResult(intent1,0);
 	       return true;
 	      
		 case R.id.action_new:
			 openCreateDialog();
			 return true;			
			
		default:
			return super.onOptionsItemSelected(item);
		}
	}

	/* *
	 * Called when invalidateOptionsMenu() is triggered
	 */
	@Override
	public boolean onPrepareOptionsMenu(Menu menu) {
		// if nav drawer is opened, hide the action items
		boolean drawerOpen = mDrawerLayout.isDrawerOpen(mDrawerList);
		menu.findItem(R.id.action_settings).setVisible(!drawerOpen);
		return super.onPrepareOptionsMenu(menu);
	}

	/**
	 * Diplaying fragment view for selected nav drawer list item
	 * */
	private void displayView(int position) {
		// update the main content by replacing fragments
		Fragment fragment = null;
		switch (position) {
		case 0:
			fragment = new ServersFragment();
			break;
		case 1:
			fragment = new ImagesFragment();
			break;
		case 2:
			fragment = new ProjectsFragment();
			break;
		case 3:
			fragment = new UsersFragment();
			break;
		case 4:
			fragment = new VolumesFragment();
			break;
		case 5:
			fragment = new NetworkFragment();
			break;
		
		case 6:
			fragment = new AccessSecurityFragment();
			break;		
		case 7:
			fragment = new LBfragment();
			break;

		default:
			break;
		}

		if (fragment != null) {
			FragmentManager fragmentManager = getFragmentManager();
			fragmentManager.beginTransaction()
					.replace(R.id.frame_container, fragment, ""+position+"").commit();

			// update selected item and title, then close the drawer
			mDrawerList.setItemChecked(position, true);
			mDrawerList.setSelection(position);
			setTitle(navMenuTitles[position]);
			mDrawerLayout.closeDrawer(mDrawerList);
		} else {
			// error in creating fragment
			Log.e("MainActivity", "Error in creating fragment");
		}
	}

	@Override
	public void setTitle(CharSequence title) {
		mTitle = title;
		getActionBar().setTitle(mTitle);
	}

	/**
	 * When using the ActionBarDrawerToggle, you must call it during
	 * onPostCreate() and onConfigurationChanged()...
	 */

	@Override
	protected void onPostCreate(Bundle savedInstanceState) {
		super.onPostCreate(savedInstanceState);
		// Sync the toggle state after onRestoreInstanceState has occurred.
		mDrawerToggle.syncState();
	}

	@Override
	public void onConfigurationChanged(Configuration newConfig) {
		super.onConfigurationChanged(newConfig);
		// Pass any configuration change to the drawer toggls
		mDrawerToggle.onConfigurationChanged(newConfig);
	}

	void openCreateDialog()
	{
		   final SharedPreferences setpreferences1 = PreferenceManager.getDefaultSharedPreferences(getApplicationContext());
		// Strings to Show In Dialog with Radio Buttons
		   String isadmin= setpreferences1.getString("isAdmin", "");
		   final String tenant_id= setpreferences1.getString("tenant_id", "");
		final CharSequence[] items = {" Instance "," Project "," Volume ", " User ", " Network ", " Security Group/Rule "};
		final CharSequence[] items1 = {" Instance "};
		                // Creating and Building the Dialog 
		                AlertDialog.Builder builder = new AlertDialog.Builder(this);
		                builder.setTitle("Add");
		                if(isadmin.equalsIgnoreCase("true"))
		                {
		                builder.setSingleChoiceItems(items, -1, new DialogInterface.OnClickListener() {
		                public void onClick(DialogInterface dialog, int item) {
		                   
		                	 Intent intent;
		                    switch(item)
		                    {
		                        case 0:
		                                // when instance option is selected
		               			       intent= new Intent(getApplicationContext(),CreateServerActivity.class);
		               			       intent.putExtra("tenant_id", tenant_id);
		               	 	           startActivityForResult(intent,0);
		                               break;
		                        case 1://  when project  option is selected
		                        	intent= new Intent(getApplicationContext(),CreateProjectActivity.class);
		               			   
		               	 	        startActivityForResult(intent,0);
		                                
		                            break;
		                  
		                        case 2:
		                                 // Your code when volume option selected    
		                        	intent= new Intent(getApplicationContext(),CreateVolumeActivity.class);
		                        	 intent.putExtra("tenant_id", tenant_id);
		               	 	        startActivityForResult(intent,0);
		                                break;
		                        case 3:
		                             intent= new Intent(getApplicationContext(),CreateUserActivity.class);
			               			   
		               	 	        startActivityForResult(intent,0);
		                        	break;
		                        case 4:
		                             intent= new Intent(getApplicationContext(),CreateNetworkActivity.class);
			               			   
		               	 	        startActivityForResult(intent,0);
		                        	break;
		                        case 5:
		                        	intent= new Intent(getApplicationContext(),CreateSecurityGroupActivity.class);
			               			   
		               	 	        startActivityForResult(intent,0);
		                        	break;
		                        
		                    }
		                    createDialog.dismiss();    
		                    }
		                });
		                }
		                else
		                {
		                	 builder.setSingleChoiceItems(items1, -1, new DialogInterface.OnClickListener() {
		 		                public void onClick(DialogInterface dialog, int item) {
		 		                   
		 		                	 Intent intent;
		 		                    switch(item)
		 		                    {
		 		                        case 0: 	        
		 		               	 	           String tenant_id= setpreferences1.getString("tenant_id", "");
		 		               	 	      
		 		               			       intent= new Intent(getApplicationContext(),CreateServerActivity.class);
		 		               			       intent.putExtra("tenant_id", tenant_id);
		 		               	 	           startActivityForResult(intent,0);
		 		                               break;
		 		                        
		 		                    }
		 		                    createDialog.dismiss();    
		 		                    }
		 		                });
		                }
		                createDialog = builder.create();
		                createDialog.show();
	}
	



}
