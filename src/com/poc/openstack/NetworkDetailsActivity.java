package com.poc.openstack;

import java.net.URL;
import java.util.HashMap;
import java.util.Map;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import com.android.volley.AuthFailureError;
import com.android.volley.NetworkError;
import com.android.volley.NetworkResponse;
import com.android.volley.NoConnectionError;
import com.android.volley.ParseError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.ServerError;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.android.volley.Response.ErrorListener;
import com.android.volley.Response.Listener;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;
import com.poc.openstack.ProjectDetailsActivity.SetAuthTokenHeader;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Fragment;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Typeface;
import android.os.AsyncTask;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.text.method.ScrollingMovementMethod;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

public class NetworkDetailsActivity extends Activity {
	
	public NetworkDetailsActivity(){}
	String url_services;
	String ip;
	String networkId;
	AlertDialog createDialog ;
	 SetAuthTokenHeader deleterequest;
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
        setContentView(R.layout.networkdetails);
      
      
      networkId=getIntent().getStringExtra("network_id");
		
		setBaseURL();
	   getNetworkDetails(networkId);
	   
       
    }
	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		getMenuInflater().inflate(R.menu.add_delete, menu);
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		
		// Handle action bar actions click
		switch (item.getItemId()) {
		case R.id.action_new:
			openSubnetCreateActivity();
			return true;
			
		 case R.id.action_delete:
			 openDeleteNetworkDialog();
			 return true;
			 		
		default:
			return super.onOptionsItemSelected(item);
		}
	}
	
	private void openSubnetCreateActivity() {
		// TODO Auto-generated method stub
		
		AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle("Add");
        final CharSequence[] items1 = {" Subnet ", " Port "};
        builder.setSingleChoiceItems(items1, -1, new DialogInterface.OnClickListener() {
             public void onClick(DialogInterface dialog, int item) {
                
             	 Intent intent;
                 switch(item)
                 {
                     case 0: 	        
            	 	           
                    	 Intent intent1= new Intent(getApplicationContext(),CreateSubnetActivity.class);
          			     intent1.putExtra("network_id", networkId);
           	            startActivityForResult(intent1,0);
                            break;
                     
                     case 1:
                    	 Intent intent2= new Intent(getApplicationContext(),CreatePortActivity.class);
          			     intent2.putExtra("network_id", networkId);
           	            startActivityForResult(intent2,0);
                     
                 }
                 createDialog.dismiss();    
                 }
             });
        createDialog = builder.create();
        createDialog.show();

		
   
		
	}
	private void openDeleteNetworkDialog()
	{
	


		AlertDialog.Builder alert = new AlertDialog.Builder(this);

		alert.setTitle("Delete Network");
		alert.setMessage("Are you sure?");



		alert.setPositiveButton("Yes", new DialogInterface.OnClickListener() {
		public void onClick(DialogInterface dialog, int whichButton) {
		 // String value = input.getText().toString();
		  // Do something with value!
		  try {
				RequestQueue requestQueue = Volley.newRequestQueue(getApplicationContext());    
				
				 
					
					URL url = new URL(url_services+"/networks/"+networkId);
					
				// System.out.println(url);
				 deleterequest= new  SetAuthTokenHeader(
				            Request.Method.DELETE,
				            url.toString().trim(),null,
				            deletesuccessListener(),
				            deleteErrorListener());
					
					requestQueue.add(deleterequest);


		        } catch (Exception e) {
		        	
		            e.printStackTrace();
		            
		    
		        }
		  }
		});

		alert.setNegativeButton("No", new DialogInterface.OnClickListener() {
		  public void onClick(DialogInterface dialog, int whichButton) {
		    // Canceled.
		  }
		});

		alert.show();
		
	}
	
	private Response.Listener<String> deletesuccessListener() {
	    return new Response.Listener<String>() {
	        @Override
	        public void onResponse(String response) {
	        	Toast.makeText(getApplicationContext(), "Delete Success!", Toast.LENGTH_SHORT).show();
	        	
	     
	        	               }
	    };

	}


	private Response.ErrorListener deleteErrorListener() {
	    return new Response.ErrorListener() {
	        @Override
	        public void onErrorResponse(VolleyError error) {
	        	error.printStackTrace();
	        	 if(deleterequest.getStatusCode()==204)
	        	 {
	               	Toast.makeText(getApplicationContext(), "Project Delete Success!", Toast.LENGTH_SHORT).show();
	               	Intent intent= new Intent(getApplicationContext(),MainActivity.class);
			        startActivityForResult(intent,0);	 
	        	 }
	               	else
	        			Toast.makeText(getApplicationContext(), "Project Delete Failed!", Toast.LENGTH_SHORT).show();
	        	//Toast.makeText(getApplicationContext(), "Delete Failed!", Toast.LENGTH_SHORT).show();
	        }
	    };
	}
	
	private void setBaseURL()
	{
	    SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(getApplicationContext());
        String host_ip = preferences.getString("host_ip","");
        if(!host_ip.equalsIgnoreCase(""))
        {
          ip= host_ip;
          url_services="http://"+ip+":9696/v2.0";
        }
        else
        {
      
       Toast.makeText(getApplicationContext(), "Please configure openstack server", Toast.LENGTH_SHORT).show();
       Intent intent= new Intent(this,SettingsActivity.class);
       startActivityForResult(intent,0);
        }
	}
	
	void getNetworkDetails(String netId)
	{
		try {
			RequestQueue requestQueue = Volley.newRequestQueue(getApplicationContext());    
			 JsonObjectRequest request = null ;	
				
				URL url = new URL(url_services+"/networks/"+netId);
			// System.out.println(url);
				request= new  SetAuthTokenHeader(
			            Request.Method.GET,
			            url.toString().trim(),null,
			            successListener(),
			            ErrorListener());
				
				requestQueue.add(request);
			
	            
	          
			

	        } catch (Exception e) {
	        	
                e.printStackTrace();
	            
	    
	        }
		
	}

	
	private Response.Listener<JSONObject> successListener() {
        return new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
            	
            	TextView txt=(TextView)findViewById(R.id.network_details);
            	TextView txt1=(TextView)findViewById(R.id.subnets);
            	Typeface typeface = Typeface.createFromAsset(getAssets(), "font/Roboto-Medium.ttf");
            	txt.setTypeface(typeface);
            	txt1.setTypeface(typeface);
               System.out.println(response.toString());
            	try {
            		 JSONObject json= response.getJSONObject("network"); 
            		String network_name=json.getString("name");
            	 	String network_id=json.getString("id");
            	 	String status=json.getString("status");
            	 	String net_type=json.getString("provider:network_type");
            	 	JSONArray subnet= json.getJSONArray("subnets");
                   
            	 	String tenantId=json.getString("tenant_id");
            	
            	 	
            	 txt.setText("Name:\n"+network_name+"\n"+
            	 	          "Id:\n"+network_id+"\n"+
            	 	          "Status:\n"+status+"\n"+
            	 	          "Project Id:\n "+tenantId+"\n"+
            	 	          "Network Type:\n"+net_type+"\n");
            	 if(subnet.length()>0)
            	 {
            	 for(int i=0;i<subnet.length();i++)
                 {
            		 String subnetid=subnet.getString(i);
                    //txt1.setText("Subnets:\n"+subnetid);
                    getNetworkSubnetDetails(subnetid);
                 }
            	 	
            	 }
            	 else
            		 txt1.setText("No Subnets for this networks");
				} catch (JSONException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
         
            	               }
        };
    
    }
    
    
    private Response.ErrorListener ErrorListener() {
        return new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
            	error.printStackTrace();
            	if( error instanceof NetworkError) {
            		Toast.makeText(getApplicationContext(), "Network Error!", Toast.LENGTH_SHORT).show();
				} else if( error instanceof ServerError) {
					Toast.makeText(getApplicationContext(), "Server Error!", Toast.LENGTH_SHORT).show();
				} else if( error instanceof AuthFailureError) {
					Toast.makeText(getApplicationContext(), "Authentication Failed!", Toast.LENGTH_SHORT).show();
				} else if( error instanceof ParseError) {
					Toast.makeText(getApplicationContext(), "Parse Error!", Toast.LENGTH_SHORT).show();
				} else if( error instanceof NoConnectionError) {
					Toast.makeText(getApplicationContext(), "No Connection Error!", Toast.LENGTH_SHORT).show();
				} else if( error instanceof TimeoutError) {
					Toast.makeText(getApplicationContext(), "Timeout Error!", Toast.LENGTH_SHORT).show();
				}
            }
        };
    }
	
	
    void getNetworkSubnetDetails(String subnetId)
	{
		try {
			RequestQueue requestQueue = Volley.newRequestQueue(getApplicationContext());    
			 JsonObjectRequest request = null ;	
				
				URL url = new URL(url_services+"/subnets/"+subnetId);
			// System.out.println(url);
				request= new  SetAuthTokenHeader(
			            Request.Method.GET,
			            url.toString().trim(),null,
			            subnetsuccessListener(),
			            subnetErrorListener());
				
				requestQueue.add(request);
			
	            
	          
			

	        } catch (Exception e) {
	        	
                e.printStackTrace();
	            
	    
	        }
		
	}

	
	private Response.Listener<JSONObject> subnetsuccessListener() {
        return new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
            	
            	TextView txt=(TextView)findViewById(R.id.network_details);
            	TextView txt1=(TextView)findViewById(R.id.subnets);
            //	txt.setMovementMethod(new ScrollingMovementMethod());
            //	txt1.setMovementMethod(new ScrollingMovementMethod());
            	Typeface typeface = Typeface.createFromAsset(getAssets(), "font/Roboto-Medium.ttf");
            	txt.setTypeface(typeface);
            	txt1.setTypeface(typeface);
            //   System.out.println(response.toString());
            	try {
            		
            		 JSONObject json= response.getJSONObject("subnet"); 
            		String image_name=json.getString("name");
            	 	String image_id=json.getString("ip_version");
            	 	String status=json.getString("cidr");
            	 	String progress=json.getString("gateway_ip");
            	 	JSONArray subnet= json.getJSONArray("allocation_pools");
        
                	
            	 if(txt1.getText().toString().equals("Loading Subnets.."))
            	 {
            	 txt1.setText("Name:\n"+image_name+"\n"+
            	 	          "IP Version:\nIPv"+image_id+"\n"+
            	 	          "CIDR:\n"+status+"\n"+
            	 	          "Gateway IP:\n "+progress+"\n"
            	 	      );
            	 }
            	 else{
            		 txt1.setText("Name:\n"+image_name+"\n"+
           	 	          "IP Version:\nIPv"+image_id+"\n"+
           	 	          "CIDR:\n"+status+"\n"+
           	 	          "Gateway IP:\n "+progress+"\n"
           	 	      );
            	 }
            		
				} catch (JSONException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
         
            	               }
        };
    
    }
    
    
    private Response.ErrorListener subnetErrorListener() {
        return new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
            	error.printStackTrace();
            	if( error instanceof NetworkError) {
            		Toast.makeText(getApplicationContext(), "Network Error!", Toast.LENGTH_SHORT).show();
				} else if( error instanceof ServerError) {
					Toast.makeText(getApplicationContext(), "Server Error!", Toast.LENGTH_SHORT).show();
				} else if( error instanceof AuthFailureError) {
					Toast.makeText(getApplicationContext(), "Authentication Failed!", Toast.LENGTH_SHORT).show();
				} else if( error instanceof ParseError) {
					Toast.makeText(getApplicationContext(), "Parse Error!", Toast.LENGTH_SHORT).show();
				} else if( error instanceof NoConnectionError) {
					Toast.makeText(getApplicationContext(), "No Connection Error!", Toast.LENGTH_SHORT).show();
				} else if( error instanceof TimeoutError) {
					Toast.makeText(getApplicationContext(), "Timeout Error!", Toast.LENGTH_SHORT).show();
				}
            }
        };
    }
	
	

	public class SetAuthTokenHeader extends JsonObjectRequest 
	{
		int mStatusCode;
	 public SetAuthTokenHeader(int method, String url, JSONObject jsonRequest,Listener listener, ErrorListener errorListener)
	 {
	   super(method, url, jsonRequest, listener, errorListener);
	 }

	 @Override
	 public Map getHeaders() throws AuthFailureError {
	   Map headers = new HashMap();
	   SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(getApplicationContext());
	   String token = preferences.getString("auth_token","");
	   headers.put("X-Auth-Token", token);
	   return headers;
	 }

	 public int getStatusCode() {
			return mStatusCode;
	    }

	    @Override
	    protected Response<JSONObject> parseNetworkResponse(NetworkResponse response) {
	        mStatusCode = response.statusCode;
	        return super.parseNetworkResponse(response);
	    }

	}
	

}
