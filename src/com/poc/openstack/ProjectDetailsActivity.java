package com.poc.openstack;

import java.net.URL;
import java.util.HashMap;
import java.util.Map;

import org.json.JSONException;
import org.json.JSONObject;

import com.android.volley.AuthFailureError;
import com.android.volley.NetworkError;
import com.android.volley.NetworkResponse;
import com.android.volley.NoConnectionError;
import com.android.volley.ParseError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.ServerError;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.android.volley.Response.ErrorListener;
import com.android.volley.Response.Listener;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;
import com.poc.openstack.UserDetailsActivity.SetAuthTokenHeader;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Fragment;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Typeface;
import android.os.AsyncTask;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.TextView;
import android.widget.Toast;

public class ProjectDetailsActivity extends Activity {
	String ip;
	String url_services;
	String ProjectId;
	 SetAuthTokenHeader	deleterequest;
    
	public ProjectDetailsActivity(){}
	
	public void onCreate(Bundle savedInstanceState) {
	   super.onCreate(savedInstanceState);
       setContentView(R.layout.projectdetails);
      
       ProjectId=getIntent().getStringExtra("project_id");
       setBaseURL();
	   getProjectDetails(ProjectId);
       
    }
	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		getMenuInflater().inflate(R.menu.delete, menu);
	
	
	       
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		
		// Handle action bar actions click
		switch (item.getItemId()) {
	      
		 case R.id.action_delete:
			 openDeleteProjectDialog();
			 return true;
			 		
		default:
			return super.onOptionsItemSelected(item);
		}
	}
	void openDeleteProjectDialog()
	{
	


		AlertDialog.Builder alert = new AlertDialog.Builder(this);

		alert.setTitle("Delete Project");
		alert.setMessage("Are you sure?");



		alert.setPositiveButton("Yes", new DialogInterface.OnClickListener() {
		public void onClick(DialogInterface dialog, int whichButton) {
		 // String value = input.getText().toString();
		  // Do something with value!
		  try {
				RequestQueue requestQueue = Volley.newRequestQueue(getApplicationContext());    
				
				 
					
					URL url = new URL(url_services+"/projects/"+ProjectId);
					
				// System.out.println(url);
				 deleterequest= new  SetAuthTokenHeader(
				            Request.Method.DELETE,
				            url.toString().trim(),null,
				            deletesuccessListener(),
				            deleteErrorListener());
					
					requestQueue.add(deleterequest);
					
				
		            
		          
				

		        } catch (Exception e) {
		        	
		            e.printStackTrace();
		            
		    
		        }
		  }
		});

		alert.setNegativeButton("No", new DialogInterface.OnClickListener() {
		  public void onClick(DialogInterface dialog, int whichButton) {
		    // Canceled.
		  }
		});

		alert.show();
		
	}
	
	private Response.Listener<String> deletesuccessListener() {
	    return new Response.Listener<String>() {
	        @Override
	        public void onResponse(String response) {
	        	Toast.makeText(getApplicationContext(), "Delete Success!", Toast.LENGTH_SHORT).show();
	        	
	     
	        	               }
	    };

	}


	private Response.ErrorListener deleteErrorListener() {
	    return new Response.ErrorListener() {
	        @Override
	        public void onErrorResponse(VolleyError error) {
	        	error.printStackTrace();
	        	 if(deleterequest.getStatusCode()==204)
	        	 {
	               	Toast.makeText(getApplicationContext(), "Project Delete Success!", Toast.LENGTH_SHORT).show();
	               	Intent intent= new Intent(getApplicationContext(),MainActivity.class);
			        startActivityForResult(intent,0);	 
	        	 }
	               	else
	        			Toast.makeText(getApplicationContext(), "Project Delete Failed!", Toast.LENGTH_SHORT).show();
	        	
	        }
	    };
	}
	
	private void setBaseURL()
	{
	    SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(getApplicationContext());
        String host_ip = preferences.getString("host_ip","");
        if(!host_ip.equalsIgnoreCase(""))
        {
          ip= host_ip;
          url_services="http://"+ip+":5000/v3";
        }
        else
        {
      
       Toast.makeText(getApplicationContext(), "Please configure openstack server to proceed", Toast.LENGTH_SHORT).show();
       Intent intent= new Intent(this,SettingsActivity.class);
       startActivityForResult(intent,0);
        }
	}
	
   void getProjectDetails(String projectId)
   {
	   try {
			RequestQueue requestQueue = Volley.newRequestQueue(getApplicationContext());    
			 JsonObjectRequest request = null ;	
				
				URL url = new URL(url_services+"/projects/"+projectId);
			 System.out.println(url);
				request= new  SetAuthTokenHeader(
			            Request.Method.GET,
			            url.toString().trim(),null,
			            successListener(),
			            ErrorListener());
				
				requestQueue.add(request);
			
	            
	          
			

	        } catch (Exception e) {
	        	
               e.printStackTrace();
	            
	    
	        }
		
   }
	private Response.Listener<JSONObject> successListener() {
        return new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
          
            	TextView txt=(TextView)findViewById(R.id.project_details);
  //             System.out.println(response.toString());
            	Typeface typeface = Typeface.createFromAsset(getAssets(), "font/Roboto-Medium.ttf");
            	txt.setTypeface(typeface);
            	try {
            		 JSONObject json= response.getJSONObject("project"); 
            		String project_name=json.getString("name");
            	 	String project_desc=json.getString("description");
            	 	String status=json.getString("enabled");
            	 	String domainId=json.getString("domain_id");
            	 	String projectid=json.getString("id");
            	 	
            	 txt.setText("Name:\n"+project_name+"\n"+
            	 	          "Description:\n "+project_desc+"\n"+
            	 	          "Enabled:\n "+status+"\n"+
            	 	          "Domain Id:\n "+domainId+"\n"+
            	 	          "Project Id:\n"+projectid+"\n" );
            		//System.out.println("Hello!!"+output);
            	 	
					
				} catch (JSONException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
         
            	               }
        };
    
    }
    
    
    private Response.ErrorListener ErrorListener() {
        return new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
            	error.printStackTrace();
            	if( error instanceof NetworkError) {
            		Toast.makeText(getApplicationContext(), "Network Error!", Toast.LENGTH_SHORT).show();
				} else if( error instanceof ServerError) {
					Toast.makeText(getApplicationContext(), "Server Error!", Toast.LENGTH_SHORT).show();
				} else if( error instanceof AuthFailureError) {
					Toast.makeText(getApplicationContext(), "Authentication Failed!", Toast.LENGTH_SHORT).show();
				} else if( error instanceof ParseError) {
					Toast.makeText(getApplicationContext(), "Parse Error!", Toast.LENGTH_SHORT).show();
				} else if( error instanceof NoConnectionError) {
					Toast.makeText(getApplicationContext(), "No Connection Error!", Toast.LENGTH_SHORT).show();
				} else if( error instanceof TimeoutError) {
					Toast.makeText(getApplicationContext(), "Timeout Error!", Toast.LENGTH_SHORT).show();
				}
            }
        };
    }
	
	public class SetAuthTokenHeader extends JsonObjectRequest 
	{
		int mStatusCode;
	 public SetAuthTokenHeader(int method, String url, JSONObject jsonRequest,Listener listener, ErrorListener errorListener)
	 {
	   super(method, url, jsonRequest, listener, errorListener);
	 }

	 @Override
	 public Map getHeaders() throws AuthFailureError {
	   Map headers = new HashMap();
	   SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(getApplicationContext());
	   String token = preferences.getString("auth_token","");
	   headers.put("X-Auth-Token", token);
	   return headers;
	 }

	 public int getStatusCode() {
	        
			
			
			return mStatusCode;
	    }

	    @Override
	    protected Response<JSONObject> parseNetworkResponse(NetworkResponse response) {
	        mStatusCode = response.statusCode;
	        return super.parseNetworkResponse(response);
	    }


	}
	

}
