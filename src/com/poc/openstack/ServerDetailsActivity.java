package com.poc.openstack;


import java.net.MalformedURLException;
import java.net.URL;
import java.util.HashMap;
import java.util.Map;

import org.apache.http.HttpStatus;
import org.json.JSONException;
import org.json.JSONObject;

import com.android.volley.AuthFailureError;
import com.android.volley.NetworkResponse;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.Response.ErrorListener;
import com.android.volley.Response.Listener;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;
import com.poc.openstack.CreateServerActivity.SetAuthTokenHeader;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Fragment;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Typeface;
import android.os.AsyncTask;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

public class ServerDetailsActivity extends Activity {
	
	public ServerDetailsActivity(){}
	private String ip;
	private String url_services;
	SetAuthTokenHeader rebootrequest = null ;
	SetAuthTokenHeader changepassrequest = null ;
	SetAuthTokenHeader deleterequest;
	 private String TenantId;
	 private String ServerId;
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
        setContentView(R.layout.serverdetails);
        TenantId=getIntent().getStringExtra("tenant_id");
		ServerId=getIntent().getStringExtra("server_id");
		String flavorId=getIntent().getStringExtra("flavor_id");
		setBaseURL();
        getServerFlavorDetails(TenantId,ServerId,flavorId);
       
    }
	
	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
	    // Inflate the menu items for use in the action bar
	    MenuInflater inflater = getMenuInflater();
	    inflater.inflate(R.menu.server_menu, menu);
	    return super.onCreateOptionsMenu(menu);
	}
	
	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
	    // Handle presses on the action bar items
	    switch (item.getItemId()) {
	     
	        case R.id.action_changep:
	        	//changePasswordDialog(TenantId,ServerId);
	            return true;
	        case R.id.action_reboot:
	        	rebootServerDialog(TenantId,ServerId);
	            return true;
	        case R.id.action_del:
	        	deleteServer();
	        	return true;
	        default:
	            return super.onOptionsItemSelected(item);
	    }
	}
	 private void deleteServer() {
		// TODO Auto-generated method stub
		 AlertDialog.Builder alert = new AlertDialog.Builder(this);

			alert.setTitle("Delete Server");
			alert.setMessage("Are you sure?");



			alert.setPositiveButton("Yes", new DialogInterface.OnClickListener() {
			public void onClick(DialogInterface dialog, int whichButton) {
			  try {
				RequestQueue requestQueue = Volley.newRequestQueue(getApplicationContext());    

						URL url = new URL(url_services+"/"+TenantId+"/servers/"+ServerId);

						deleterequest= new  SetAuthTokenHeader(
					            Request.Method.DELETE,
					            url.toString().trim(),null,
					            deletesuccessListener(),
					            deleteErrorListener());
						
						requestQueue.add(deleterequest);

			        } catch (Exception e) {
			        	
			            e.printStackTrace();
			            
			    
			        }
			  }
			});

			alert.setNegativeButton("No", new DialogInterface.OnClickListener() {
			  public void onClick(DialogInterface dialog, int whichButton) {
			    // Canceled.
			  }
			});

			alert.show();
			
		}

		private Response.Listener<String> deletesuccessListener() {
		    return new Response.Listener<String>() {
		        @Override
		        public void onResponse(String response) {
		        	Toast.makeText(getApplicationContext(), "Delete Success!", Toast.LENGTH_SHORT).show();
		        	Intent intent= new Intent(getApplicationContext(),MainActivity.class);
			        startActivityForResult(intent,0);	 
		     
		        	               }
		    };

		}


		private Response.ErrorListener deleteErrorListener() {
		    return new Response.ErrorListener() {
		        @Override
		        public void onErrorResponse(VolleyError error) {
		        	error.printStackTrace();
		        	 if(deleterequest.getStatusCode()==204){
		        		 	Toast.makeText(getApplicationContext(), "Delete Server Success!", Toast.LENGTH_SHORT).show(); 
		        		 	Intent intent= new Intent(getApplicationContext(),MainActivity.class);
					        startActivityForResult(intent,0);	 
		        	 }
		        	 else
		        			Toast.makeText(getApplicationContext(), "Delete Server Failed!", Toast.LENGTH_SHORT).show();
		        	//Toast.makeText(getApplicationContext(), "Delete Failed!", Toast.LENGTH_SHORT).show();
		        }
		    };
		}


	private void setBaseURL()
		{
		    SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(getApplicationContext());
	        String host_ip = preferences.getString("host_ip","");
	        if(!host_ip.equalsIgnoreCase(""))
	        {
	          ip= host_ip;
	          url_services="http://"+ip+":8774/v2";
	        }
	        else
	        {
	      
	       Toast.makeText(getApplicationContext(), "Please configure openstack server to proceed", Toast.LENGTH_SHORT).show();
	       Intent intent= new Intent(this,SettingsActivity.class);
	       startActivityForResult(intent,0);
	        }
		}
	 
	void getServerFlavorDetails(String tenantId, String ServerId,String flavorId)
	{
		try {
			RequestQueue requestQueue = Volley.newRequestQueue(getApplicationContext());    
			 JsonObjectRequest serverDetailsRequest = null ;	
			 JsonObjectRequest flavorDetailsRequest = null ;	
				URL url = new URL(url_services+"/"+tenantId+"/servers/"+ServerId);
				URL url1 = new URL(url_services+"/"+tenantId+"/flavors/"+flavorId);
				
				serverDetailsRequest= new  SetAuthTokenHeader(
			            Request.Method.GET,
			            url.toString().trim(),null,
			            serverSuccessListener(),
			            serverErrorListener());
				
				requestQueue.add(serverDetailsRequest);
			
			
				flavorDetailsRequest= new  SetAuthTokenHeader(
			            Request.Method.GET,
			            url1.toString().trim(),null,
			            flavorSuccessListener(),
			            flavorErrorListener());
				
				requestQueue.add(flavorDetailsRequest);
	          
			

	        } catch (Exception e) {
	        	
                e.printStackTrace();
	            
	    
	        }
	}
	private Response.Listener<JSONObject> serverSuccessListener() {
        return new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
            	String fixed=null;
            	String floating=null;
            	TextView txt=(TextView)findViewById(R.id.details);
            	Typeface typeface = Typeface.createFromAsset(getAssets(), "font/Roboto-Medium.ttf");
            	txt.setTypeface(typeface);
               //System.out.println(response.toString());
            	try {
            		 JSONObject json= response.getJSONObject("server"); 
            		String server_name=json.getString("name");
            	 	String server_id=json.getString("id");
            	 	String status=json.getString("status");
            	 	JSONObject addr=json.getJSONObject("addresses");
            	 	if(addr.has("global"))
            	 	{
            	 	int length=addr.getJSONArray("global").length();
            	 	if(length==2)
            	 	{
            	 	fixed=addr.getJSONArray("global").getJSONObject(0).getString("addr");
            	    floating=addr.getJSONArray("global").getJSONObject(1).getString("addr");
            	 	}
            	 	else if(length==1)
            	 	{
            	 	fixed=addr.getJSONArray("global").getJSONObject(0).getString("addr");
            	 	}
            	 	}
            	 	if(fixed!=null && floating != null)
            	 	{
            	 txt.setText("Name:\n "+server_name+"\n"+
            	 	          "Id:\n "+server_id+"\n"+
            	 	          "Status:\n "+status+"\n"+
            	 	          "Fixed IP:\n "+fixed+"\n"+
            	 	          "Floating IP:\n"+floating+"\n");
            		//System.out.println("Hello!!"+output);
            	 	}
            	 	else if(fixed == null)
            	 	{
            	 		 txt.setText("Name:\n "+server_name+"\n"+
	            	 	          "Id:\n "+server_id+"\n"+
	            	 	          "Status:\n "+status+"\n"+
	            	 	          "Fixed IP:\n Nil\n"+
	            	 	          "Floating IP:\n"+floating+"\n");
            	 	}
            	 	else
            	 	{
            	 		 txt.setText("Name:\n "+server_name+"\n"+
	            	 	          "Id:\n "+server_id+"\n"+
	            	 	          "Status:\n "+status+"\n"+
	            	 	          "Fixed IP:\n "+fixed+"\n"+
	            	 	          "Floating IP:\nNil\n");
            	 	}
					
				} catch (JSONException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
         
            	               }
        };
    
    }
    
    
    private Response.ErrorListener serverErrorListener() {
        return new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
            	error.printStackTrace();
            	Toast.makeText(getApplicationContext(), "Fetch Failed!", Toast.LENGTH_SHORT).show();
            }
        };
    }
	
	
	private Response.Listener<JSONObject> flavorSuccessListener() {
        return new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
         
            	TextView txt=(TextView)findViewById(R.id.flavordetails);
            	Typeface typeface = Typeface.createFromAsset(getAssets(), "font/Roboto-Medium.ttf");
            	txt.setTypeface(typeface);
              // System.out.println(response.toString());
            	try {
            		 JSONObject json= response.getJSONObject("flavor"); 
            		String name=json.getString("name");
            	 	String ram=json.getString("ram");
            	 	String vcpus=json.getString("vcpus");
            	 	String id= json.getString("id");
            	 	
            	 txt.setText("Flavor Name: "+name+"\n"+
            	 	          "Flavor Id: "+id+"\n"+
            	 	          "RAM: "+ram+" MB\n"+
            	 	          "VCPUs: "+vcpus+"\n");
            		//System.out.println("Hello!!"+output);
            	 
					
				} catch (JSONException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
         
            	               }
        };
    
    }
    
    
    private Response.ErrorListener flavorErrorListener() {
        return new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
            	error.printStackTrace();
            	Toast.makeText(getApplicationContext(), "Fetch Failed!", Toast.LENGTH_SHORT).show();
            }
        };
    }
    void changePassword(String tenant_id,String server_id,String newPassword)
    {

   	RequestQueue requestQueue = Volley.newRequestQueue(getApplicationContext());   		
		JSONObject json = new JSONObject();
		JSONObject json1 = new JSONObject();
		try{				        
     // Add a JSON Object
		

     json.put( "adminPass",newPassword);
     
     json1.put("changePassword",json);        
     // Passing a number to toString() adds indentation
     System.out.println( json1.toString(2) );
		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		URL url;
		try {
			url = new URL(url_services+"/"+tenant_id+"/servers/"+server_id+"/action");
			changepassrequest= new SetAuthTokenHeader(
		            Request.Method.POST,
		            url.toString().trim(),json1,
		            changePasswordSuccessListener(),
		            changePasswordErrorListener());
			requestQueue.add(changepassrequest);
		} catch (MalformedURLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
	
    }
	private Response.Listener<JSONObject> changePasswordSuccessListener() {
        return new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
         
            	
               System.out.println(response.toString());
            	
            	Toast.makeText(getApplicationContext(), response.toString(), Toast.LENGTH_SHORT).show();
            	               }
        };
    
    }
    
    
    private Response.ErrorListener changePasswordErrorListener() {
        return new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
            	error.printStackTrace();
            	 if(changepassrequest.getStatusCode()==202)
                   	Toast.makeText(getApplicationContext(), "Password Changed Successfully!", Toast.LENGTH_SHORT).show();
              else
      			Toast.makeText(getApplicationContext(), "Password Change Failed!", Toast.LENGTH_SHORT).show();   
                     
                     }
          
        };
    }
	void changePasswordDialog(final String tenant_id,final String server_id)
	{
		AlertDialog.Builder alert = new AlertDialog.Builder(this);

		alert.setTitle("Change Server Password");
		alert.setMessage("Enter New Password");

		// Set an EditText view to get user input 
		final EditText input = new EditText(this);
		alert.setView(input);

		alert.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
		public void onClick(DialogInterface dialog, int whichButton) {
		  String value = input.getText().toString();
		  // Do something with value!
		  changePassword(tenant_id,server_id,value);
		  }
		});

		alert.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
		  public void onClick(DialogInterface dialog, int whichButton) {
		    // Canceled.
		  }
		});

		alert.show();
	}
	void rebootServerDialog(final String tenantId,final String serverId)
	{
		AlertDialog.Builder alert = new AlertDialog.Builder(this);

		alert.setTitle("Reboot");
		alert.setMessage("Are you sure?");

		// Set an EditText view to get user input 
		

		alert.setPositiveButton("Yes", new DialogInterface.OnClickListener() {
		public void onClick(DialogInterface dialog, int whichButton) {
		 
		  // Do something with value!
		  rebootServer(tenantId,serverId);
		  }
		});

		alert.setNegativeButton("No", new DialogInterface.OnClickListener() {
		  public void onClick(DialogInterface dialog, int whichButton) {
		    // Canceled.
		  }
		});

		alert.show();
	}
	void rebootServer(String tenantId,String serverId){
		
		 
			
		   	RequestQueue requestQueue = Volley.newRequestQueue(getApplicationContext());   
				
				
				
				
				JSONObject json = new JSONObject();
				JSONObject json1 = new JSONObject();
				try{				        
		     // Add a JSON Object
				

		     json.put( "type","SOFT");
		     
		     json1.put("reboot",json);        
		     // Passing a number to toString() adds indentation
		     System.out.println( json1.toString(2) );
				} catch (JSONException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				URL url;
				try {
					url = new URL(url_services+"/"+tenantId+"/servers/"+serverId+"/action");
					rebootrequest= new SetAuthTokenHeader(
				            Request.Method.POST,
				            url.toString().trim(),json1,
				            rebootSuccessListener(),
				            rebootErrorListener());
					requestQueue.add(rebootrequest);
				} catch (MalformedURLException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				
		
	}
	
	private Response.Listener<NetworkResponse> rebootSuccessListener() {
        return new Response.Listener<NetworkResponse>() {
            @Override
            public void onResponse(NetworkResponse response) {
       
           
            	
            	Toast.makeText(getApplicationContext(), response.data.toString(), Toast.LENGTH_SHORT).show();
            	               
         
         }
        };
    
    }
    
    
    private Response.ErrorListener rebootErrorListener() {
        return new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
         if(rebootrequest.getStatusCode()==202)
              	Toast.makeText(getApplicationContext(), "Reboot Success!", Toast.LENGTH_SHORT).show();
         else
 			Toast.makeText(getApplicationContext(), "Reboot Failed!", Toast.LENGTH_SHORT).show();   
                
                }
        };
    }
	
	public class SetAuthTokenHeader extends JsonObjectRequest 
	{
		int mStatusCode;
	 public SetAuthTokenHeader(int method, String url, JSONObject jsonRequest,Listener listener, ErrorListener errorListener)
	 {
	   super(method, url, jsonRequest, listener, errorListener);
	 }

	 @Override
	 public Map getHeaders() throws AuthFailureError {
	   Map headers = new HashMap();
	   SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(getApplicationContext());
	   String token = preferences.getString("auth_token","");
	   headers.put("X-Auth-Token", token);
	   return headers;
	 }
	 
	 public int getStatusCode() {
	        
			return mStatusCode;
	    }

	    @Override
	    protected Response<JSONObject> parseNetworkResponse(NetworkResponse response) {
	        mStatusCode = response.statusCode;
	        return super.parseNetworkResponse(response);
	    }
	    

	}
	

}
