package com.poc.openstack;


import java.net.URL;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.logging.Logger;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import com.android.volley.AuthFailureError;
import com.android.volley.NetworkError;
import com.android.volley.NoConnectionError;
import com.android.volley.ParseError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.ServerError;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.android.volley.Response.ErrorListener;
import com.android.volley.Response.Listener;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;
import com.poc.openstack.adapter.CustomListViewAdapter;
import com.poc.openstack.model.ServerListItem;

import android.app.Fragment;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.drawable.Drawable;
import android.os.AsyncTask;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.v4.app.ListFragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.AbsListView;
import android.widget.AbsListView.OnScrollListener;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

public class ServersFragment extends Fragment {
	
	public ServersFragment(){}
	   private static final int RESULTS_PAGE_SIZE = 10;
	   String ip;
	String url_services;
	  private List<String> mDataSourceList = new ArrayList<String>();  
	  private ArrayList<ServerListItem> mEntries = new ArrayList<ServerListItem>();
	  ListView listView ;
CustomListViewAdapter adapter;
JSONArray responsejson;
	@Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
            Bundle savedInstanceState) {
 
        View rootView = inflater.inflate(R.layout.servers_list, container, false);
        TextView tv= (TextView) rootView.findViewById(R.id.loadingServers);
        tv.setVisibility(View.VISIBLE);
        setBaseURL();
        fetchInstances();
        
        return rootView;
    }
	 @Override 
	    public void onActivityCreated(Bundle savedInstanceState) {  
	        super.onActivityCreated(savedInstanceState);  
	           
	        //add data to ListView  
	      listView = (ListView) getActivity().findViewById(R.id.listofServers);  
 	      adapter=new CustomListViewAdapter(getActivity(),0,mEntries);  
	       listView.setAdapter(adapter);    
	       listView.setTextFilterEnabled(true);
	    
	       
	           
	    }  
	 
	 private void setBaseURL()
		{
		    SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(getActivity().getApplicationContext());
	        String host_ip = preferences.getString("host_ip","");
	        if(!host_ip.equalsIgnoreCase(""))
	        {
	          ip= host_ip;
	          url_services="http://"+ip+":8774/v2";
	        }
	        else
	        {
	      
	       Toast.makeText(getActivity().getApplicationContext(), "Please configure openstack server", Toast.LENGTH_SHORT).show();
	       Intent intent= new Intent(getActivity(),SettingsActivity.class);
	       startActivityForResult(intent,0);
	        }
		}
	 void fetchInstances()
	 {
		 try {
				RequestQueue requestQueue = Volley.newRequestQueue(getActivity().getApplicationContext());    
				int startIndex = 1 + mEntries.size();
				SetAuthTokenHeader request = null ;	
				
				 SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(getActivity());
				   String tenant_id = preferences.getString("tenant_id","");
			
				
					URL url = new URL(url_services+"/"+tenant_id+"/servers/detail");
					request= new  SetAuthTokenHeader(
				            Request.Method.GET,
				            url.toString().trim(),null,
				            successListener(),
				            ErrorListener());
					
					requestQueue.add(request);
				
		            
		          
				

		        } catch (Exception e) {
		        	
                 e.printStackTrace();
		            
		    
		        }
			
			
		  
		}
		private Response.Listener<JSONObject> successListener() {
	        return new Response.Listener<JSONObject>() {
	            @Override
	            public void onResponse(JSONObject response) {
	            	//TextView txt=(TextView)getView().findViewById(R.id.label);
	            	//System.out.println(response.toString());
	            	//txt.setText(response.toString());
	            	 TextView tv= (TextView) getView().findViewById(R.id.loadingServers);
	                 tv.setVisibility(View.INVISIBLE);
	            	 String name = null ;
	            	 String Subtext = null;
	            	 TextView ntv= (TextView)getActivity().findViewById(R.id.text);
	            	 Drawable image = null;
	            	try {
	            		responsejson= response.getJSONArray("servers");
	            		JSONObject entry;
	            		
	            		//System.out.println(responsejson.length());
	            		if(responsejson.length() !=0)
	            		{
	                    for (int i = 0; i < responsejson.length(); i++) {
	                        entry = responsejson.getJSONObject(i);  
	                         name = entry.getString("name");
	                         String stat=entry.getString("status");
	                       Subtext =entry.getString("status")+" | "+entry.getString("updated");
	                       if(stat.equalsIgnoreCase("active"))
	                       {
	                    	image  =getResources().getDrawable(R.drawable.ic_action_accept);
	                       }
	                       else if(stat.equalsIgnoreCase("error"))
	                    	   image  =getResources().getDrawable(R.drawable.ic_action_error);
	                       else if(stat.equalsIgnoreCase("suspended"))
	                    	   image=getResources().getDrawable(R.drawable.ic_action_warning);
	                       else if(stat.equalsIgnoreCase("reboot"))
	                    	   image=getResources().getDrawable(R.drawable.ic_action_refresh);
	                       else if(stat.equalsIgnoreCase("shutoff"))
	                    	   image=getResources().getDrawable(R.drawable.ic_action_cancel);
	                       else
	                    	   image=getResources().getDrawable(R.drawable.ic_action_warning);
	                       mEntries.add(new ServerListItem(name,Subtext,image));
	                        }
	                        
	                       
	                    
	                    adapter.notifyDataSetChanged();
	            		}
	            		else
	            		{
	            			ntv.setVisibility(View.VISIBLE);
	            		}
					            }
					 catch (JSONException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
	            	
	            	 listView.setOnItemClickListener(new ListView.OnItemClickListener(){

	         			@Override
	         			public void onItemClick(AdapterView<?> parent, View view,
	         					int position, long id) {
	         				// TODO Auto-generated method stub
	         				JSONObject jsonPass=new JSONObject();
	         				
	         				  Intent intent= new Intent(getActivity(),ServerDetailsActivity.class);
							  try {
								  jsonPass=responsejson.getJSONObject(position); 
							//	  System.out.println(jsonPass.getString("tenant_id")+"|||"+jsonPass.getString("id"));
								intent.putExtra("tenant_id", jsonPass.getString("tenant_id"));
								 intent.putExtra("server_id", jsonPass.getString("id"));
								 intent.putExtra("flavor_id",jsonPass.getJSONObject("flavor").getString("id"));
							} catch (JSONException e) {
								// TODO Auto-generated catch block
								e.printStackTrace();
							}
							 
	         				  startActivity(intent);
			         
	         			}}) ;
	       // Intent intent= new Intent(getActivity().getApplicationContext(),MainActivity.class);
					//startActivityForResult(intent,0);
	         
	            	               }
	        };
	    
	    }
	    
	    
	    private Response.ErrorListener ErrorListener() {
	        return new Response.ErrorListener() {
	            @Override
	            public void onErrorResponse(VolleyError error) {
	            	error.printStackTrace();
	            	if( error instanceof NetworkError) {
	            		 
	            		Toast.makeText(getActivity().getApplicationContext(), "Network Error! Please Login again", Toast.LENGTH_SHORT).show();
	            		SharedPreferences setpreferences = PreferenceManager.getDefaultSharedPreferences(getActivity().getApplicationContext());
	        	        SharedPreferences.Editor editor = setpreferences.edit();
	        	        editor.putString("uname", "");
	        	        editor.putString("auth_token","");
	        	        editor.putString("tenant_id","");
	        	       editor.putString("isAdmin","");
	        	        editor.commit();
	            		Intent intent= new Intent(getActivity(),LoginActivity.class);
	            		startActivityForResult(intent,0);
					} else if( error instanceof ServerError) {
						Toast.makeText(getActivity().getApplicationContext(), "Server Error!", Toast.LENGTH_SHORT).show();
					} else if( error instanceof AuthFailureError) {
						Toast.makeText(getActivity().getApplicationContext(), "Authentication Failed!", Toast.LENGTH_SHORT).show();
						Intent intent= new Intent(getActivity(),LoginActivity.class);
	            		startActivityForResult(intent,0);
					} else if( error instanceof ParseError) {
						Toast.makeText(getActivity().getApplicationContext(), "Parse Error!", Toast.LENGTH_SHORT).show();
					} else if( error instanceof NoConnectionError) {
						Toast.makeText(getActivity().getApplicationContext(), "No Connection Error!", Toast.LENGTH_SHORT).show();
					} else if( error instanceof TimeoutError) {
						Toast.makeText(getActivity().getApplicationContext(), "Timeout Error!", Toast.LENGTH_SHORT).show();
					}
	            }
	        };
	    }
	 

	public class SetAuthTokenHeader extends JsonObjectRequest 
	{
	 public SetAuthTokenHeader(int method, String url, JSONObject jsonRequest,Listener listener, ErrorListener errorListener)
	 {
	   super(method, url, jsonRequest, listener, errorListener);
	 }

	 @Override
	 public Map getHeaders() throws AuthFailureError {
	   Map headers = new HashMap();
	   SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(getActivity());
	   String token = preferences.getString("auth_token","");
	 //  System.out.println("token: "+token);
	   headers.put("X-Auth-Token", token);
	   return headers;
	 }

	}
	
	} 


