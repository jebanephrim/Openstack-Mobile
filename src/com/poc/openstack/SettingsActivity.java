package com.poc.openstack;



import java.util.regex.Matcher;
import java.util.regex.Pattern;

import android.app.Activity;
import android.app.Fragment;
import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

public class SettingsActivity extends Activity {
	 final String PREFS_NAME = "HostIP";
	 Button button;
	public SettingsActivity(){}
	private static final String PATTERN ="^(([01]?\\d\\d?|2[0-4]\\d|25[0-5])\\.){3}([01]?\\d\\d?|2[0-4]\\d|25[0-5])$";
	
	public static boolean validate(final String ip){          

	      Pattern pattern = Pattern.compile(PATTERN);
	      Matcher matcher = pattern.matcher(ip);
	      return matcher.matches();             
	}
	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.settingspage);
		getActionBar().setDisplayHomeAsUpEnabled(true);
		 SharedPreferences preferences =PreferenceManager.getDefaultSharedPreferences(this);
		String host_ip = preferences.getString("host_ip","");
		TextView tv=(TextView)findViewById(R.id.TextView);
		//String[] split = host_ip.split("\\.");
		
		tv.setText("Current host : "+host_ip);
		 final EditText updateIP= (EditText)findViewById(R.id.editText1);
		 
		 button=(Button)findViewById(R.id.button123);     
		 button.setOnClickListener(new OnClickListener() {
             public void onClick(View v) {
            	 if(!updateIP.getText().toString().isEmpty())
            			 {
            	 SharedPreferences spreferences =PreferenceManager.getDefaultSharedPreferences(getApplicationContext());
              	  SharedPreferences.Editor editor = spreferences.edit();
              	  editor.putString("host_ip",updateIP.getText().toString().trim());
              	  editor.commit();
              	Toast.makeText(getApplicationContext(), "Updated host to "+spreferences.getString("host_ip", ""), Toast.LENGTH_SHORT).show();
            			 }
            	 else
            		 Toast.makeText(getApplicationContext(), "Enter a valid host name or ip", Toast.LENGTH_SHORT).show();
            			 }
         });
 
    }
	
}
