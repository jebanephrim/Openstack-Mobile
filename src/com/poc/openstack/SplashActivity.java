package com.poc.openstack;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.HashMap;
import java.util.Map;

import org.json.JSONObject;

import com.android.volley.AuthFailureError;
import com.android.volley.NetworkError;
import com.android.volley.NetworkResponse;
import com.android.volley.NoConnectionError;
import com.android.volley.ParseError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.ServerError;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.android.volley.Response.ErrorListener;
import com.android.volley.Response.Listener;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;
import com.poc.openstack.LoginActivity.GetStatusCodeResponse;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;
import android.os.Handler;
import android.preference.PreferenceManager;
import android.view.View;
import android.view.Window;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;
 
public class SplashActivity extends Activity {
	private static int SPLASH_TIME_OUT = 3000;
	RequestQueue requestQueue;
	 GetStatusCodeResponse tokenvalidaterequest = null ;
    LinearLayout llStats;
    TextView txtPlayCount, txtEarned;
    String ip;
    String username;
	String url_services;
    private boolean isNetworkAvailable() {
        ConnectivityManager connectivityManager 
              = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo activeNetworkInfo = connectivityManager.getActiveNetworkInfo();
        return activeNetworkInfo != null && activeNetworkInfo.isConnected();
    }
    
    
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.splash);
       
        final TextView txt= (TextView) findViewById(R.id.textView1);
        final ProgressBar prog= (ProgressBar) findViewById(R.id.progressBar1);
        prog.setVisibility(View.VISIBLE);
        setBaseURL();
        txt.setText("Testing Network Connection ..");
        
        if(isNetworkAvailable())
        {
        	txt.setText("Connection ok ! \nLooking for token ..");
        	SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(getApplicationContext());
   		 String host_ip = preferences.getString("host_ip","");
   	        String token = preferences.getString("auth_token","");
   	       username= preferences.getString("uname","");
   	        if(!host_ip.equalsIgnoreCase("") && !token.equals(""))
   	        {
   	        txt.setText("Validating available token ..");
   	        validToken(token);
   	        //	Intent intent= new Intent(getApplicationContext(),MainActivity.class);
   		      //  startActivityForResult(intent,0);
   	        	
   	        }
   	        else
   	        {
        	 new Handler().postDelayed(new Runnable() {
        		 
                 /*
                  * Showing splash screen with a timer. This will be useful when you
                  * want to show case your app logo / company
                  */
      
                 @Override
                 public void run() {
                     // This method will be executed once the timer is over
                     // Start your app main activity
                	 txt.setText("No token Available. Please Login");
                	 prog.setVisibility(View.INVISIBLE);
                     Intent i = new Intent(getApplicationContext(), LoginActivity.class);
                     startActivity(i);
      
                     // close this activity
                     finish();
                 }
             }, SPLASH_TIME_OUT);
        }
        }
        else
        {
        	txt.setText("Please check your internet connection");
            prog.setVisibility(View.INVISIBLE);
        }
    }
    
    public void login(View view)
    {
    	Intent intent= new Intent(getApplicationContext(),LoginActivity.class);
	      startActivityForResult(intent,0);
    }
    
    private void validToken(String token)
	{
		requestQueue = Volley.newRequestQueue(getApplicationContext());    
		 
		URL url;
		try {
			url = new URL(url_services+"/"+token);
		
			tokenvalidaterequest= new  GetStatusCodeResponse(
		            Request.Method.GET,
		            url.toString().trim(),null,
		            sListener(),
		            eListener());
		} catch (MalformedURLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		
		requestQueue.add(tokenvalidaterequest);

		
	}
	
	private Response.Listener<JSONObject> sListener() {
	       return new Response.Listener<JSONObject>() {
	           @Override
	           public void onResponse(JSONObject response) {
	        	   final TextView txt= (TextView) findViewById(R.id.textView1);
	        	   final ProgressBar prog= (ProgressBar) findViewById(R.id.progressBar1);
	        	   txt.setText("Validation Success");
	        	   prog.setVisibility(View.INVISIBLE);
	             //success
	        Toast.makeText(getApplicationContext(), "Welcome back "+username+" !", Toast.LENGTH_SHORT).show();
	       // dialog1.dismiss();
	              	Intent intent= new Intent(getApplicationContext(),MainActivity.class);
			        startActivityForResult(intent,0);
	          
	           	               }
	       };
	   
	   }
	   
	   
	   private Response.ErrorListener eListener() {
	       return new Response.ErrorListener() {
	           @Override
	           public void onErrorResponse(VolleyError error) {
	        	   //dialog1.dismiss();
	           	error.printStackTrace();
	        	final TextView txt= (TextView) findViewById(R.id.textView1);
	           	final ProgressBar prog= (ProgressBar) findViewById(R.id.progressBar1);
	            txt.setText("Validation Failed");
	            prog.setVisibility(View.INVISIBLE);
	       /*     if(tokenvalidaterequest.getStatusCode()==202 || tokenvalidaterequest.getStatusCode()==200)
	            {
	              	Toast.makeText(getApplicationContext(), "Signing in with token", Toast.LENGTH_SHORT).show();
	              	Intent intent= new Intent(getApplicationContext(),MainActivity.class);
			        startActivityForResult(intent,0);

	            }
	         else{*/
	           
	           	if( error instanceof NetworkError) {
	        	
	        	  
	 			Toast.makeText(getApplicationContext(), "Token Expired! Please login again", Toast.LENGTH_SHORT).show();   
	 			SharedPreferences setpreferences = PreferenceManager.getDefaultSharedPreferences(getApplicationContext());
    	        SharedPreferences.Editor editor = setpreferences.edit();
    	        editor.putString("uname", "");
    	        editor.putString("auth_token","");
    	        editor.putString("tenant_id","");
    	       editor.putString("isAdmin","");
    	        editor.commit();
	            Intent intent= new Intent(getApplicationContext(),LoginActivity.class);
		        startActivityForResult(intent,0);
	       //  }
	           	} else if( error instanceof ServerError) {
					Toast.makeText(getApplicationContext(), "Server Error!", Toast.LENGTH_SHORT).show();
				} else if( error instanceof AuthFailureError) {
					Toast.makeText(getApplicationContext(), "Authentication Failed!", Toast.LENGTH_SHORT).show();
					Intent intent= new Intent(getApplicationContext(),LoginActivity.class);
            		startActivityForResult(intent,0);
				} else if( error instanceof ParseError) {
					Toast.makeText(getApplicationContext(), "Parse Error!", Toast.LENGTH_SHORT).show();
				} else if( error instanceof NoConnectionError) {
					Toast.makeText(getApplicationContext(), "No Connection Error!", Toast.LENGTH_SHORT).show();
				} else if( error instanceof TimeoutError) {
					Toast.makeText(getApplicationContext(), "Timeout Error!", Toast.LENGTH_SHORT).show();
				}
            }
        
	     
	           
	       };
	   }
	   
		private void setBaseURL()
		{
		    SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(getApplicationContext());
	        String host_ip = preferences.getString("host_ip","");
	        if(!host_ip.equalsIgnoreCase(""))
	        {
	          ip= host_ip;
	          url_services="http://"+ip+":5000/v2.0/tokens";
	        }
	        else
	        {
	      
	       Toast.makeText(getApplicationContext(), "Please configure openstack server", Toast.LENGTH_SHORT).show();
	       Intent intent= new Intent(this,SettingsActivity.class);
	       startActivityForResult(intent,0);
	        }
		}
		
	   
	   public class GetStatusCodeResponse extends JsonObjectRequest 
		{
			int mStatusCode;
		 public GetStatusCodeResponse(int method, String url, JSONObject jsonRequest,Listener listener, ErrorListener errorListener)
		 {
		   super(method, url, jsonRequest, listener, errorListener);
		 }
		 @Override
		 public Map getHeaders() throws AuthFailureError {
		   Map headers = new HashMap();
		   SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(getApplicationContext());
		   String token = preferences.getString("auth_token","");
		   headers.put("X-Auth-Token", token);
		   return headers;
		 }
		 
		 public int getStatusCode() {
		        
				return mStatusCode;
		    }

		    @Override
		    protected Response<JSONObject> parseNetworkResponse(NetworkResponse response) {
		        mStatusCode = response.statusCode;
		        return super.parseNetworkResponse(response);
		    }

		}
		

}