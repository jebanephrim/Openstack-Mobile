package com.poc.openstack;

import java.net.MalformedURLException;
import java.net.URL;
import java.util.HashMap;
import java.util.Map;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import com.android.volley.AuthFailureError;
import com.android.volley.NetworkError;
import com.android.volley.NetworkResponse;
import com.android.volley.NoConnectionError;
import com.android.volley.ParseError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.ServerError;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.android.volley.Response.ErrorListener;
import com.android.volley.Response.Listener;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;
import com.poc.openstack.model.ServerListItem;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Fragment;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Typeface;
import android.os.AsyncTask;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

public class UserDetailsActivity extends Activity {
	
	public UserDetailsActivity(){}
	 private SetAuthTokenHeader deleterequest = null ;
	String ip;
	String url_services;
	String UserId;
	String projid;
	RequestQueue requestQueue;
	public void onCreate(Bundle savedInstanceState) {
	   super.onCreate(savedInstanceState);
       setContentView(R.layout.userdetails);
     
      UserId=getIntent().getStringExtra("user_id");
       setBaseURL();
	   getUserDetails(UserId);
	   
       
    }
	
	private void setBaseURL()
	{
	    SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(getApplicationContext());
        String host_ip = preferences.getString("host_ip","");
        if(!host_ip.equalsIgnoreCase(""))
        {
          ip= host_ip;
          url_services="http://"+ip+":5000/v3";
        }
        else
        {
      
       Toast.makeText(getApplicationContext(), "Please configure openstack server to proceed", Toast.LENGTH_SHORT).show();
       Intent intent= new Intent(this,SettingsActivity.class);
       startActivityForResult(intent,0);
        }
	}
	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
	    // Inflate the menu items for use in the action bar
	    MenuInflater inflater = getMenuInflater();
	    inflater.inflate(R.menu.delete, menu);
	    return super.onCreateOptionsMenu(menu);
	}
	
	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
	    // Handle presses on the action bar items
	    switch (item.getItemId()) {
	     
	        case R.id.action_del:
	            deleteUser(UserId);
	            return true;
	        default:
	            return super.onOptionsItemSelected(item);
	    }
	}
void deleteUser(final String userId)
{


	AlertDialog.Builder alert = new AlertDialog.Builder(this);

	alert.setTitle("Delete User");
	alert.setMessage("Are you sure?");



	alert.setPositiveButton("Yes", new DialogInterface.OnClickListener() {
	public void onClick(DialogInterface dialog, int whichButton) {
	 // String value = input.getText().toString();
	  // Do something with value!
	  try {
			requestQueue = Volley.newRequestQueue(getApplicationContext());    
			
			 
				
				URL url = new URL(url_services+"/users/"+userId);
				
			// System.out.println(url);
				deleterequest= new  SetAuthTokenHeader(
			            Request.Method.DELETE,
			            url.toString().trim(),null,
			            deletesuccessListener(),
			            deleteErrorListener());
				
				requestQueue.add(deleterequest);
				
			
	            
	          
			

	        } catch (Exception e) {
	        	
	            e.printStackTrace();
	            
	    
	        }
	  }
	});

	alert.setNegativeButton("No", new DialogInterface.OnClickListener() {
	  public void onClick(DialogInterface dialog, int whichButton) {
	    // Canceled.
	  }
	});

	alert.show();
	
}

private Response.Listener<String> deletesuccessListener() {
    return new Response.Listener<String>() {
        @Override
        public void onResponse(String response) {
        	Toast.makeText(getApplicationContext(), "Delete Success!"+response, Toast.LENGTH_SHORT).show();
        	Intent intent= new Intent(getApplicationContext(),MainActivity.class);
	        startActivityForResult(intent,0);	 
     
        	               }
    };

}


private Response.ErrorListener deleteErrorListener() {
    return new Response.ErrorListener() {
        @Override
        public void onErrorResponse(VolleyError error) {
        	error.printStackTrace();
        	 if(deleterequest.getStatusCode()==204){
        		 	Toast.makeText(getApplicationContext(), "Delete Success!", Toast.LENGTH_SHORT).show(); 
        		 	Intent intent= new Intent(getApplicationContext(),MainActivity.class);
			        startActivityForResult(intent,0);	 
        	 }
        	 else
        			Toast.makeText(getApplicationContext(), "Delete Failed!", Toast.LENGTH_SHORT).show();
        	//Toast.makeText(getApplicationContext(), "Delete Failed!", Toast.LENGTH_SHORT).show();
        }
    };
}


void getUserDetails(String userId)
{
	try {
		requestQueue = Volley.newRequestQueue(getApplicationContext());    
		 JsonObjectRequest request = null ;
		 JsonObjectRequest request1 = null ;
		 
			
			URL url = new URL(url_services+"/users/"+userId);
			URL url1=new URL(url_services+"/users/"+userId+"/projects");
			
		// System.out.println(url);
			request= new  SetAuthTokenHeader(
		            Request.Method.GET,
		            url.toString().trim(),null,
		            successListener(),
		            ErrorListener());
			request1= new  SetAuthTokenHeader(
		            Request.Method.GET,
		            url1.toString().trim(),null,
		            projsuccessListener(),
		            projErrorListener());
			
			
			requestQueue.add(request);
			requestQueue.add(request1);
			
		
            
          
		

        } catch (Exception e) {
        	
            e.printStackTrace();
            
    
        }
	
	
}
private Response.Listener<JSONObject> successListener() {
    return new Response.Listener<JSONObject>() {
        @Override
        public void onResponse(JSONObject response) {
      
        	TextView txt=(TextView)findViewById(R.id.user_details);
        	Typeface typeface = Typeface.createFromAsset(getAssets(), "font/Roboto-Medium.ttf");
        	txt.setTypeface(typeface);
//             System.out.println(response.toString());
        	try {
        		 JSONObject json= response.getJSONObject("user"); 
        		String project_name=json.getString("name");
        	 	String email=json.getString("email");
        	 	String status=json.getString("enabled");
        	 	String domainId=json.getString("domain_id");
        	 	String userid=json.getString("id");
        	 	
        	 txt.setText("Name:\n"+project_name+"\n"+
        	 	          "E-mail:\n "+email+"\n"+
        	 	          "Enabled:\n "+status+"\n"+
        	 	          "Domain Id:\n "+domainId+"\n"+
        	 	          "User Id:\n"+userid+"\n" );
        		//System.out.println("Hello!!"+output);
        	 	
				
			} catch (JSONException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
     
        	               }
    };

}


private Response.ErrorListener ErrorListener() {
    return new Response.ErrorListener() {
        @Override
        public void onErrorResponse(VolleyError error) {
        	error.printStackTrace();
        	Toast.makeText(getApplicationContext(), "Fetch Failed!", Toast.LENGTH_SHORT).show();
        }
    };
}


private Response.Listener<JSONObject> projsuccessListener() {
    return new Response.Listener<JSONObject>() {
        @Override
        public void onResponse(JSONObject response) {
        	requestQueue = Volley.newRequestQueue(getApplicationContext());  
      JSONArray responsejson;
      JsonObjectRequest request2 = null ;
        	TextView txt=(TextView)findViewById(R.id.proj_list);
        	Typeface typeface = Typeface.createFromAsset(getAssets(), "font/Roboto-Medium.ttf");
        	txt.setTypeface(typeface);
             System.out.println(response.toString());
        	try {
        		
        		if(response.getJSONArray("projects").length()!=0)
        		{
        		responsejson= response.getJSONArray("projects");
        		JSONObject entry;
        		//System.out.println(responsejson.length());
                for (int i = 0; i < responsejson.length(); i++) {
                    entry = responsejson.getJSONObject(i);  
                     String name = entry.getString("name");
                     projid=entry.getString("id");
                    txt.setText("* Name: "+name+"\n"+
          	 	          "  Project Id:\n"+projid+"\n" );
                   
                  
                  
                    }
                URL url2=new URL("http://"+ip+":5000/v3/projects/"+projid+"​/users​/"+UserId+"/roles");
                request2= new  SetAuthTokenHeader(
    		            Request.Method.GET,
    		            url2.toString().trim(),null,
    		            rolesuccessListener(),
    		            roleErrorListener());
               // requestQueue.add(request2);
        		}
        		else
        			 txt.setText("No projects assigned to this user");
				
			} catch (JSONException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (MalformedURLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
        	
     
        	               }
    };

}


private Response.ErrorListener projErrorListener() {
    return new Response.ErrorListener() {
        @Override
        public void onErrorResponse(VolleyError error) {
        	error.printStackTrace();
        	if( error instanceof NetworkError) {
        		Toast.makeText(getApplicationContext(), "Network Error!", Toast.LENGTH_SHORT).show();
			} else if( error instanceof ServerError) {
				Toast.makeText(getApplicationContext(), "Server Error!", Toast.LENGTH_SHORT).show();
			} else if( error instanceof AuthFailureError) {
				Toast.makeText(getApplicationContext(), "Authentication Failed!", Toast.LENGTH_SHORT).show();
			} else if( error instanceof ParseError) {
				Toast.makeText(getApplicationContext(), "Parse Error!", Toast.LENGTH_SHORT).show();
			} else if( error instanceof NoConnectionError) {
				Toast.makeText(getApplicationContext(), "No Connection Error!", Toast.LENGTH_SHORT).show();
			} else if( error instanceof TimeoutError) {
				Toast.makeText(getApplicationContext(), "Timeout Error!", Toast.LENGTH_SHORT).show();
			}
        }
    };
}

private Response.Listener<JSONObject> rolesuccessListener() {
    return new Response.Listener<JSONObject>() {
        @Override
        public void onResponse(JSONObject response) {
        	JSONArray responsejson;
        	TextView txt=(TextView)findViewById(R.id.role_list);
        	Typeface typeface = Typeface.createFromAsset(getAssets(), "font/Roboto-Medium.ttf");
        	txt.setTypeface(typeface);
             System.out.println("Roles"+response.toString());
        	try {
        		if(response.getJSONArray("").length()!=0)
        		{
        		responsejson=response.getJSONArray("");
        		JSONObject entry;
        		//System.out.println(responsejson.length());
                for (int i = 0; i < response.length(); i++) {
                    entry = responsejson.getJSONObject(i);  
                     String name = entry.getString("name");
                    String id=entry.getString("id");
                    txt.setText("* Name: "+name+"\n"+
          	 	          "  Role Id:\n"+id+"\n" );
                   
                  
                  
                    }
        		}
        		else
        			 txt.setText("No roles assigned to this user");
				
			} catch (JSONException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
     
        	               }
    };

}


private Response.ErrorListener roleErrorListener() {
    return new Response.ErrorListener() {
        @Override
        public void onErrorResponse(VolleyError error) {
        	error.printStackTrace();
        	if( error instanceof NetworkError) {
        		Toast.makeText(getApplicationContext(), "Network Error!", Toast.LENGTH_SHORT).show();
			} else if( error instanceof ServerError) {
				Toast.makeText(getApplicationContext(), "Server Error!", Toast.LENGTH_SHORT).show();
			} else if( error instanceof AuthFailureError) {
				Toast.makeText(getApplicationContext(), "Authentication Failed!", Toast.LENGTH_SHORT).show();
			} else if( error instanceof ParseError) {
				Toast.makeText(getApplicationContext(), "Parse Error!", Toast.LENGTH_SHORT).show();
			} else if( error instanceof NoConnectionError) {
				Toast.makeText(getApplicationContext(), "No Connection Error!", Toast.LENGTH_SHORT).show();
			} else if( error instanceof TimeoutError) {
				Toast.makeText(getApplicationContext(), "Timeout Error!", Toast.LENGTH_SHORT).show();
			}
        }
    };
}

	public class SetAuthTokenHeader extends JsonObjectRequest 
	{
		int mStatusCode;
	 public SetAuthTokenHeader(int method, String url, JSONObject jsonRequest,Listener listener, ErrorListener errorListener)
	 {
	   super(method, url, jsonRequest, listener, errorListener);
	 }

	 @Override
	 public Map getHeaders() throws AuthFailureError {
	   Map headers = new HashMap();
	   SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(getApplicationContext());
	   String token = preferences.getString("auth_token","");
	   headers.put("X-Auth-Token", token);
	   return headers;
	 }
	 public int getStatusCode() {
	        
			
			return mStatusCode;
	    }

	    @Override
	    protected Response<JSONObject> parseNetworkResponse(NetworkResponse response) {
	        mStatusCode = response.statusCode;
	        return super.parseNetworkResponse(response);
	    }


	
	}
	

}
