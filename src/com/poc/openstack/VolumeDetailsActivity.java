package com.poc.openstack;

import java.net.MalformedURLException;
import java.net.URL;
import java.util.HashMap;
import java.util.Map;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import com.android.volley.AuthFailureError;
import com.android.volley.NetworkError;
import com.android.volley.NetworkResponse;
import com.android.volley.NoConnectionError;
import com.android.volley.ParseError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.ServerError;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.android.volley.Response.ErrorListener;
import com.android.volley.Response.Listener;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;
import com.poc.openstack.model.ServerListItem;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Fragment;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Typeface;
import android.os.AsyncTask;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

public class VolumeDetailsActivity extends Activity {
	
	public VolumeDetailsActivity(){}
	 private SetAuthTokenHeader deleterequest = null ;
	String ip;
	String url_services;
	String volumeId;
	String tenantid;
	String projid;
	RequestQueue requestQueue;
	public void onCreate(Bundle savedInstanceState) {
	   super.onCreate(savedInstanceState);
       setContentView(R.layout.volumedetails);
     
       volumeId=getIntent().getStringExtra("volume_id");
       tenantid=getIntent().getStringExtra("tenant_id");
       setBaseURL();
	   getVolumeDetails(volumeId,tenantid);
	   
       
    }
	
	private void setBaseURL()
	{
	    SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(getApplicationContext());
        String host_ip = preferences.getString("host_ip","");
        if(!host_ip.equalsIgnoreCase(""))
        {
          ip= host_ip;
          url_services="http://"+ip+":8776/v2";
        }
        else
        {
      
       Toast.makeText(getApplicationContext(), "Please configure openstack server to proceed", Toast.LENGTH_SHORT).show();
       Intent intent= new Intent(this,SettingsActivity.class);
       startActivityForResult(intent,0);
        }
	}
	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
	    // Inflate the menu items for use in the action bar
	    MenuInflater inflater = getMenuInflater();
	    inflater.inflate(R.menu.delete, menu);
	    return super.onCreateOptionsMenu(menu);
	}
	
	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
	    // Handle presses on the action bar items
	    switch (item.getItemId()) {
	     
	        case R.id.action_del:
	            deleteVolume();
	            return true;
	        default:
	            return super.onOptionsItemSelected(item);
	    }
	}
void deleteVolume()
{


	AlertDialog.Builder alert = new AlertDialog.Builder(this);

	alert.setTitle("Delete Volume");
	alert.setMessage("Are you sure?");



	alert.setPositiveButton("Yes", new DialogInterface.OnClickListener() {
	public void onClick(DialogInterface dialog, int whichButton) {
	 // String value = input.getText().toString();
	  // Do something with value!
	  try {
			requestQueue = Volley.newRequestQueue(getApplicationContext());    
			
			 
				
				URL url = new URL(url_services+"/"+tenantid+"/volumes/"+volumeId);
				
			// System.out.println(url);
				deleterequest= new  SetAuthTokenHeader(
			            Request.Method.DELETE,
			            url.toString().trim(),null,
			            deletesuccessListener(),
			            deleteErrorListener());
				
				requestQueue.add(deleterequest);
				
			
	            
	          
			

	        } catch (Exception e) {
	        	
	            e.printStackTrace();
	            
	    
	        }
	  }
	});

	alert.setNegativeButton("No", new DialogInterface.OnClickListener() {
	  public void onClick(DialogInterface dialog, int whichButton) {
	    // Canceled.
	  }
	});

	alert.show();
	
}

private Response.Listener<String> deletesuccessListener() {
    return new Response.Listener<String>() {
        @Override
        public void onResponse(String response) {
        	Toast.makeText(getApplicationContext(), "Delete Success!", Toast.LENGTH_SHORT).show();
        	
     
        	               }
    };

}


private Response.ErrorListener deleteErrorListener() {
    return new Response.ErrorListener() {
        @Override
        public void onErrorResponse(VolleyError error) {
        	error.printStackTrace();
        	 if(deleterequest.getStatusCode()==202){
               	Toast.makeText(getApplicationContext(), "Delete Volume Success!", Toast.LENGTH_SHORT).show();
               	Intent intent= new Intent(getApplicationContext(),MainActivity.class);
		        startActivityForResult(intent,0);	 
        	 }else
        			Toast.makeText(getApplicationContext(), "Delete Volume Failed!", Toast.LENGTH_SHORT).show();
        	//Toast.makeText(getApplicationContext(), "Delete Failed!", Toast.LENGTH_SHORT).show();
        }
    };
}


void getVolumeDetails(String volumeId,String tenantId)
{
	try {
		requestQueue = Volley.newRequestQueue(getApplicationContext());    
		 JsonObjectRequest request = null ;			
			URL url = new URL(url_services+"/"+tenantId+"/volumes/"+volumeId);

			request= new  SetAuthTokenHeader(
		            Request.Method.GET,
		            url.toString().trim(),null,
		            successListener(),
		            ErrorListener());
		
			requestQueue.add(request);


        } catch (Exception e) {
        	
            e.printStackTrace();
            
    
        }
	
	
}
private Response.Listener<JSONObject> successListener() {
    return new Response.Listener<JSONObject>() {
        @Override
        public void onResponse(JSONObject response) {
      
        	TextView txt=(TextView)findViewById(R.id.vol_details);
        	Typeface typeface = Typeface.createFromAsset(getAssets(), "font/Roboto-Medium.ttf");
        	txt.setTypeface(typeface);
//             System.out.println(response.toString());
        	try {
        		 JSONObject json= response.getJSONObject("volume"); 
        		String project_name=json.getString("name");
        	 	String desc=json.getString("description");
        	 	String status=json.getString("status");
        	 	String azone=json.getString("availability_zone");
        	 	String volid=json.getString("id");
        	 	String bootable=json.getString("bootable");
        	 	String size=json.getString("size");
        	 	
        	 txt.setText("Name:\n"+project_name+"\n"+
        	 	          "Description:\n "+desc+"\n"+
        	 	          "Status:\n "+status+"\n"+
        	 	          "Availability Zone:\n "+azone+"\n"+
        	 	         "Size:\n"+size+"\n"+ 
        	 	          "Volume Id:\n"+volid+"\n"+ 
        	 	          "Bootable:\n"+bootable);
        		//System.out.println("Hello!!"+output);
        	 	
				
			} catch (JSONException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
     
        	               }
    };

}


private Response.ErrorListener ErrorListener() {
    return new Response.ErrorListener() {
        @Override
        public void onErrorResponse(VolleyError error) {
        	error.printStackTrace();
        	Toast.makeText(getApplicationContext(), "Fetch Failed!", Toast.LENGTH_SHORT).show();
        }
    };
}




	public class SetAuthTokenHeader extends JsonObjectRequest 
	{
		int mStatusCode;
	 public SetAuthTokenHeader(int method, String url, JSONObject jsonRequest,Listener listener, ErrorListener errorListener)
	 {
	   super(method, url, jsonRequest, listener, errorListener);
	 }

	 @Override
	 public Map getHeaders() throws AuthFailureError {
	   Map headers = new HashMap();
	   SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(getApplicationContext());
	   String token = preferences.getString("auth_token","");
	   headers.put("X-Auth-Token", token);
	   return headers;
	 }
	 public int getStatusCode() {
	        
			
			return mStatusCode;
	    }

	    @Override
	    protected Response<JSONObject> parseNetworkResponse(NetworkResponse response) {
	        mStatusCode = response.statusCode;
	        return super.parseNetworkResponse(response);
	    }


	
	}
	

}
