package com.poc.openstack.adapter;

import java.util.List;

import com.poc.openstack.R;
import com.poc.openstack.model.ServerListItem;



import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;


public class CustomListViewAdapter extends ArrayAdapter<ServerListItem> {

    
    public CustomListViewAdapter(Context context, 
                              int textViewResourceId, 
                              List<ServerListItem> objects
                              ) {
        super(context, textViewResourceId, objects);
     //   mImageLoader = imageLoader;
    }

    
    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View v = convertView;

        if (v == null) {
            LayoutInflater vi = (LayoutInflater) this.getContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            v = vi.inflate(R.layout.list_style_overview, null);
        }
        
        ViewHolder holder = (ViewHolder) v.getTag(R.id.id_holder);       
        
        if (holder == null) {
            holder = new ViewHolder(v);
            v.setTag(R.id.id_holder, holder);
        }        
        
        ServerListItem entry = getItem(position);
        
        holder.title.setText(entry.getTitle());
        holder.subtext.setText(entry.getSubtext());
        holder.image.setImageDrawable(entry.getImage());
        return v;
    }
    
    
    private class ViewHolder {
        TextView subtext;
        TextView title; 
        ImageView image;
        
        public ViewHolder(View v) {
            subtext = (TextView) v.findViewById(R.id.sublabel);
            title = (TextView) v.findViewById(R.id.label);
            image =(ImageView) v.findViewById(R.id.icon);
            v.setTag(this);
        }
    }
}
