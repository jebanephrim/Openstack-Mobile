package com.poc.openstack.adapter;
import java.util.ArrayList;

import com.poc.openstack.R;
import com.poc.openstack.model.FlavorsSpinnerItem;
import com.poc.openstack.model.ImagesSpinnerItem;
 
import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;
 
public class FlavorsSpinnerAdapter extends ArrayAdapter<FlavorsSpinnerItem> {
    private Activity context;
    ArrayList<FlavorsSpinnerItem> data = null;
    LayoutInflater inflater;
    FlavorsSpinnerItem tempValues;
    public FlavorsSpinnerAdapter(Activity context, int resource,
            ArrayList<FlavorsSpinnerItem> data) {
        super(context, resource, data);
        this.context = context;
        this.data = data;
        inflater = (LayoutInflater)context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }
 
    @Override
    public View getDropDownView(int position, View convertView,ViewGroup parent) {
        return getCustomView(position, convertView, parent);
    }
 
    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        return getCustomView(position, convertView, parent);
    }
 
    // This funtion called for each row ( Called data.size() times )
    public View getCustomView(int position, View convertView, ViewGroup parent) {
 
        /********** Inflate spinner_rows.xml file for each row ( Defined below ) ************/
        View row = inflater.inflate(R.layout.simplespinner, parent, false);
         
        /***** Get each Model object from Arraylist ********/
        tempValues = null;
        tempValues = (FlavorsSpinnerItem) data.get(position);
         
        TextView label        = (TextView)row.findViewById(R.id.item_value);
        TextView sub          = (TextView)row.findViewById(R.id.item_id);
       
        /*if(position==0){
             
            // Default selected Spinner item 
            label.setText("-Select Flavor-");
            sub.setText("");
        }
        else
        {*/
            // Set values for spinner each row 
            label.setText(tempValues.getTitle());
            sub.setText(tempValues.getImageId());
           
             
     //   }   
 
        return row;
    }
}