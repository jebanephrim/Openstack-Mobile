package com.poc.openstack.adapter;
import java.util.ArrayList;

import com.poc.openstack.R;
import com.poc.openstack.model.ImagesSpinnerItem;
 
import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;
 
public class ImagesSpinnerAdapter extends ArrayAdapter<ImagesSpinnerItem> {
    private Activity context;
    ArrayList<ImagesSpinnerItem> data = null;
    LayoutInflater inflater;
    ImagesSpinnerItem tempValues;
    public ImagesSpinnerAdapter(Activity context, int resource,
            ArrayList<ImagesSpinnerItem> data) {
        super(context, resource, data);
        this.context = context;
        this.data = data;
        inflater = (LayoutInflater)context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }
 
    @Override
    public View getDropDownView(int position, View convertView,ViewGroup parent) {
        return getCustomView(position, convertView, parent);
    }
 
    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        return getCustomView(position, convertView, parent);
    }
 
    // This funtion called for each row ( Called data.size() times )
    public View getCustomView(int position, View convertView, ViewGroup parent) {
 
        /********** Inflate spinner_rows.xml file for each row ( Defined below ) ************/
        View row = inflater.inflate(R.layout.simplespinner, parent, false);
         
        /***** Get each Model object from Arraylist ********/
        tempValues = null;
        tempValues = (ImagesSpinnerItem) data.get(position);
         
        TextView label        = (TextView)row.findViewById(R.id.item_value);
        TextView sub          = (TextView)row.findViewById(R.id.item_id);
       
     /*   if(position==0){
             
            // Default selected Spinner item 
            label.setText("-Select Image-");
            sub.setText("");
        }
        else
        {*/
            // Set values for spinner each row 
            label.setText(tempValues.getTitle());
            sub.setText(tempValues.getImageId());
           
             
      //  }   
 
        return row;
    }
}