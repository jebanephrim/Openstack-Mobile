package com.poc.openstack.model;



public class ImagesSpinnerItem {
    private String mTitle;
    private String mId;
    


    public ImagesSpinnerItem(String title, String id ) {
        super();
        mTitle = title;
        mId=id;
        
        
    }


    public String getTitle() {
        return this.mTitle;
    }


   


	public String getImageId() {
		return this.mId;
	}


	public void setImageId(String mId) {
		this.mId = mId;
	}
}