package com.poc.openstack.model;



public class KeypairSpinnerItem {
    private String mTitle;
    private String mId;
    


    public KeypairSpinnerItem(String title, String id ) {
        super();
        mTitle = title;
        mId=id;
        
        
    }


    public String getTitle() {
        return this.mTitle;
    }


   


	public String getImageId() {
		return this.mId;
	}


	public void setImageId(String mId) {
		this.mId = mId;
	}
}