package com.poc.openstack.model;



public class NetworkItem {
    private String mTitle;
    private String mId;
    


    public NetworkItem(String title, String id ) {
        super();
        mTitle = title;
        mId=id;
        
        
    }


    public String getTitle() {
        return this.mTitle;
    }


   


	public String getNetworkId() {
		return this.mId;
	}


	public void setNetworkId(String mId) {
		this.mId = mId;
	}
}