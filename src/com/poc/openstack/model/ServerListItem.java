package com.poc.openstack.model;

import android.graphics.drawable.Drawable;
import android.widget.ImageView;


public class ServerListItem {
    private String mTitle;
    private String mSubtext;
    private Drawable mImage;


    public ServerListItem(String title, String subtext,Drawable image ) {
        super();
        mTitle = title;
        mSubtext = subtext;
        setImage(image);
        
    }


    public String getTitle() {
        return mTitle;
    }


    public String getSubtext() {
        return mSubtext;
    }


	public Drawable getImage() {
		return mImage;
	}


	public void setImage(Drawable mImage) {
		this.mImage = mImage;
	}
}